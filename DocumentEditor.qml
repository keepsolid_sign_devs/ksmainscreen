import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import Theme 1.0
import com.keepsolid.app.paintview 1.0


KSDocumentController {

    id: documentController

//******************* test preview ******************************************************************
    Image {
        id: testPreviewImage
        z: 200
        x: 0
        y: 0
        width: 200
        height: 300

        Rectangle {
            id: visualTag
            anchors.right: testPreviewImage.right
            anchors.top: testPreviewImage.top
            width: 50
            height: 50
            color: "#ff0000"
            visible: false
        }

        Component.onCompleted: {
            documentController.activatePage(4);
        }

        Connections {
            target: documentController
            onPageReady: {
                if(pageIndex == 4)
                {
                    visualTag.visible = documentController.page(pageIndex).currentUserAnnotationsCount > 0;
                    documentController.page(pageIndex).grabWithImage(testPreviewImage);
                }
            }
        }
    }
//*************************************************************************************

    onImageChanged: {
        var vPage = page(pageIndex);
        console.log("onImageChanged: ", pageIndex, " ", image," := ",vPage);
        if(vPage != null)
        {
            vPage.image = image;
        }
    }

//    Connections {
//        target: documentController
//        onImageReadyChanged: {
//
//        }
//    }

    property var currentActiveAnnotation : null
    property var contextMenu : null
    property var contextMenuLevel;
    property point scaleOrigin : Qt.point(documentController.width/2,0)

    onDocumentScaleChanged: {
        if(docViewLoader.item)
        {
            for(var i = 0; i < documentController.pdf.pageCount; i++)
            {
                if(docViewLoader.item.isActive(i))
                {
                    documentController.setPageActivationIndex(i);
                }
            }
        }
    }

    function page(pageIndex){
        return docViewLoader.item ? docViewLoader.item.page(pageIndex) : null;
    }

    function activatePage(pageIndex) {

        if(docViewLoader.item != null)
        {
            docViewLoader.item.activatePage(pageIndex);
        }
    }

    onCurrentActiveAnnotationChanged: {

        if(documentController.currentActiveAnnotation != null)
        {
            switch(documentController.currentActiveAnnotation.kernel.type)
            {
            case KSAnnotationItem.AnnotationDate:
            case KSAnnotationItem.AnnotationText:

                dropdown.toolsTitles = [
                            ["Edit", KSDocumentController.AnnotationContextEdit, false],
                            ["Font", KSDocumentController.AnnotationContextFont, false],
                            ["Change signer", KSDocumentController.AnnotationContextChangeParticipant, false],
                            ["Delete", KSDocumentController.AnnotationContextDelete, false]
                        ];
                break;
            case KSAnnotationItem.AnnotationSignature:
            case KSAnnotationItem.AnnotationInitials:

                dropdown.toolsTitles = [
                            ["Edit", KSDocumentController.AnnotationContextEdit, false],
                            ["Change signer", KSDocumentController.AnnotationContextChangeParticipant, false],
                            ["Delete", KSDocumentController.AnnotationContextDelete, false]
                        ];
                break;
            default:
                break;
            }
        }
        else
        {

        }
    }

    onWidthChanged: {
        if(contextMenu != null)
            contextMenu.updateFrame();
    }
    onHeightChanged: {
        if(contextMenu != null)
            contextMenu.updateFrame();
    }
    onXChanged: {
        if(contextMenu != null)
            contextMenu.updateFrame();
    }
    onYChanged: {
        if(contextMenu != null)
            contextMenu.updateFrame();
    }

    function zoom(scale)
    {
        var diff = scale - documentController.documentScale;


        //        documentScrollGround.contentX = documentScrollGround.contentX + documentScrollGround.width*diff
        //        documentScrollGround.contentY = documentScrollGround.contentY + documentScrollGround.height*diff

        console.log(scale);
        //        documentController.scaleOrigin = origin;
        documentController.documentScale = scale;
    }

    Rectangle {
        id: groundView
        objectName: "groundView"
        color: "#ebebeb"
        anchors.fill: parent

        transform: Scale {
            id: docScaleTransform
            xScale: documentController.documentScale
            yScale: documentController.documentScale
            origin.x: documentController.scaleOrigin.x
            origin.y: documentController.scaleOrigin.y
        }

        Loader {
            id: docViewLoader
            anchors.fill: groundView

            active: appController.docController != null && appController.docController.pdf.status == QPdfDocument.Ready
            source: "CSDocumentScrollView.qml"

        }

        CSDateEditWindow {
            id: dateEditWindow

            onDateChanged: {
                if(documentController.currentActiveAnnotation != null)
                {
                    documentController.currentActiveAnnotation.kernel.kernelData = Qt.formatDate(dateEditWindow.date, dateEditWindow.dateTextFormat);
                }
            }

            function updateFrame()
            {
                if(documentController.currentActiveAnnotation == null)
                {
                    return;
                }

                var x = documentController.currentActiveAnnotation.mapToItem(null, 0, 0).x
                        + (documentController.currentActiveAnnotation.width - dateEditWindow.width)*0.5
                        + mainWindow.x
                var y = documentController.currentActiveAnnotation.mapToItem(null, 0, 0).y
                        + documentController.currentActiveAnnotation.height + 4
                        + mainWindow.y

                updatePosition(x,y);
            }
        }

        AnnotationContextMenu {
            id: dropdown

            function updateFrame()
            {
                if(documentController.currentActiveAnnotation == null)
                {
                    return;
                }

                var x = documentController.currentActiveAnnotation.mapToItem(null, 0, 0).x
                        + (documentController.currentActiveAnnotation.width - dropdown.width)*0.5
                        + mainWindow.x
                var y = documentController.currentActiveAnnotation.mapToItem(null, 0, 0).y
                        - dropdown.height
                        + mainWindow.y

                updatePosition(x,y);
            }

            onItemDidClick: {
                switch(index)
                {
                case KSDocumentController.AnnotationContextChangeParticipant:
                    dropdown.hide(false);
                    break;
                case KSDocumentController.AnnotationContextDelete:
                    dropdown.hide(false);
                    break;
                case KSDocumentController.AnnotationContextEdit:
                    dropdown.hide(false);

                    if(documentController.currentActiveAnnotation.kernel.type == KSAnnotationItem.AnnotationDate)
                    {
                        contextMenu = dateEditWindow
                        dateEditWindow.date = Date.fromLocaleString(Qt.locale(),
                                                                    documentController.currentActiveAnnotation.kernel.kernelData,
                                                                    dateEditWindow.dateTextFormat);
                        contextMenu.updateFrame();
                        contextMenu.showDelayed();
                    }
                    else if(documentController.currentActiveAnnotation.kernel.type == KSAnnotationItem.AnnotationText)
                    {
                        dropdown.hide(false);
                        textEditWindow.editText(documentController.currentActiveAnnotation.kernel.kernelData);
                    }
                    else
                    {
                        dropdown.hide(false);
                        paintWindow.paintSignature(documentController.currentActiveAnnotation.kernel.kernelData);
                    }

                    break;
                case KSDocumentController.AnnotationContextFont:

                    dropdown.toolsTitles = [
                                ["Bold", KSDocumentController.AnnotationContextBold, documentController.currentActiveAnnotation.kernel.isBold],
                                ["Italic", KSDocumentController.AnnotationContextItalic, documentController.currentActiveAnnotation.kernel.isItalic],
                                ["A+", KSDocumentController.AnnotationContextAAdd, false],
                                ["A-", KSDocumentController.AnnotationContextASub, false]
                            ];

                    break;
                case KSDocumentController.AnnotationContextBold:
                    sender.isTurnOn  = !sender.isTurnOn;
                    documentController.currentActiveAnnotation.kernel.isBold = sender.isTurnOn;
                    break;
                case KSDocumentController.AnnotationContextItalic:
                    sender.isTurnOn  = !sender.isTurnOn;
                    documentController.currentActiveAnnotation.kernel.isItalic = sender.isTurnOn;
                    break;
                case KSDocumentController.AnnotationContextAAdd:
                    documentController.currentActiveAnnotation.kernel.fontSize += 1;
                    break;
                case KSDocumentController.AnnotationContextASub:
                    if(documentController.currentActiveAnnotation.kernel.fontSize < 300)
                    {
                        documentController.currentActiveAnnotation.kernel.fontSize -= 1;
                    }
                    else if(documentController.currentActiveAnnotation.kernel.fontSize > 0)
                    {
                        documentController.currentActiveAnnotation.kernel.fontSize += 1;
                    }

                    break;
                }
            }
        }

    }


}
