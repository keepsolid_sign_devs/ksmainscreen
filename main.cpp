#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "AppController.h"
#include "qqmlcontext.h"
#include <QQuickView>
#include "theme.h"
#include "KSPaintView.h"
#include "ksAnnotationItem.h"
//#include "imageprovider.h"
#include "ksimageprovider.h"
#include "kspageitem.h"
#include "KSAnnotationListModel.h"
#include "KSDocumentController.h"
#include "KSAnnotationBorder.h"
#include "KSAnnotationText.h"
#include "KSAnnotationDate.h"
#include "KSAsyncPageProvider.h"
#include "KSImageDrawer.h"

#include <QPdfDocument>

void showChildrensNames(QObject *qmlObject)
{
    QObjectList list = qmlObject->children();
    qDebug() << "Parent:" << qmlObject->objectName();
    foreach( QObject* item, list )
    {
        qDebug() << item->objectName();
        showChildrensNames(item);
    }
}

int main(int argc, char *argv[])
{
    qsrand(QTime::currentTime().msec());

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterType<KSPaintView>("com.keepsolid.app.paintview", 1, 0, "KSPaintView");
    qmlRegisterType<KSAnnotationBorder>("com.keepsolid.app.paintview", 1, 0, "KSAnnotationBorder");
    qmlRegisterType<KSAnnotationItem>("com.keepsolid.app.paintview", 1, 0, "KSAnnotationItem");
    qmlRegisterType<KSAnnotationText>("com.keepsolid.app.paintview", 1, 0, "KSAnnotationText");
    qmlRegisterType<KSAnnotationDate>("com.keepsolid.app.paintview", 1, 0, "KSAnnotationDate");
    qmlRegisterType<KSAnnotationListModel>("com.keepsolid.app.paintview", 1, 0, "KSAnnotationListModel");
    qmlRegisterType<KSPageItem>("com.keepsolid.app.paintview", 1, 0, "KSPageItem");
    qmlRegisterType<KSDocumentController>("com.keepsolid.app.paintview", 1, 0, "KSDocumentController");
    qmlRegisterType<KSImageDrawer>("com.keepsolid.app.paintview", 1, 0, "KSImageDrawer");

    qmlRegisterType<QPdfDocument>("com.keepsolid.app.paintview", 1, 0, "QPdfDocument");

    qmlRegisterSingletonType<Theme>("Theme", 1, 0, "Theme", &Theme::qmlInstance);

    QObject::connect(&engine, SIGNAL(quit()), QCoreApplication::instance(), SLOT(quit()));

    engine.rootContext()->setContextProperty("appController", AppController::instance());

    engine.addImageProvider("myprovider",KSImageProvider::instance());
    engine.addImageProvider("page_preview",KSAsyncPageProvider::instance());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

//    QObject *rootObject = engine.rootObjects().first();
//    KSDocumentController *docController = (KSDocumentController *)rootObject->findChild<QObject*>("documentEditor");
//    AppController::instance()->setDocController(docController);

//    qDebug() << rootObject->objectName();

    return app.exec();
}
