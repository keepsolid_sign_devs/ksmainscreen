import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import Qt.labs.calendar 1.0

Window {

    id: window
    width: 100
    height: 150
    flags: Qt.Tool | Qt.WindowStaysOnTopHint

    ListView {
        id: listview

        width: 200; height: 200
        snapMode: ListView.SnapOneItem
        orientation: ListView.Horizontal
        highlightRangeMode: ListView.StrictlyEnforceRange

        model: CalendarModel {
            from: new Date(2015, 0, 1)
            to: new Date(2015, 11, 31)
        }

        delegate: MonthGrid {
            width: listview.width
            height: listview.height

            month: model.month
            year: model.year
            locale: Qt.locale("en_US")
        }

        ScrollIndicator.horizontal: ScrollIndicator { }
    }
}
