import QtQuick 2.0
import "../../ui" as KSControls
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

Window {
    id: window
    width: 520
    height: 435
//    flags: Qt.Tool | Qt.WindowStaysOnTopHint
    modality: Qt.WindowModal
    title: "Draw Signature"
    property var source: ""
    property bool accepted : false

    function paintSignature(src)
    {
        window.source = src;
        window.accepted = false;
        show();
    }

    function paintClose()
    {
        hide();
    }

    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 76
        anchors.top: parent.top
        anchors.topMargin: 20

        KSControls.CSPaintView {
            id: paintView
            anchors.fill: parent
            source: window.source
            painter: ""
        }
    }

    Button {
        id: button
        width: 100
        text: qsTr("Ok")
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.top: rectangle.bottom
        anchors.topMargin: 20
        onClicked: {
            window.source = paintView.save(0);
            window.accepted = true;
            paintClose();
        }
    }

    Button {
        id: button1
        width: 100
        text: qsTr("Cancel")
        anchors.top: rectangle.bottom
        anchors.topMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: button.left
        anchors.rightMargin: 10
        onClicked: {
            paintClose();
        }
    }
}
