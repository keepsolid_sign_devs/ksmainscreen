import QtQuick 2.0
import QtGraphicalEffects 1.0
import Theme 1.0
import com.keepsolid.app.paintview 1.0
//import "ImageProviderCore"



KSPageItem {
    id: page

    //    color: "#00000000"

    property int inset: 10
    property alias annotations : annotationRepeater
    property var pageWidth: 0
    property var pageHeight: 0
    //    property var scale: 1
    property var image
    signal annotationDidActivate(var sender)
    signal annotationDidPress(var sender)
    signal annotationDidRelease(var sender)
    signal pageReady()


    onImageChanged: {
        pageImage.image = page.image;
    }

    //    onScaleChanged: {
    //        for(var i = 0; i < annotationRepeater.count; i++)
    //        {
    //            var item = annotationRepeater.itemAt(i).item;

    //            item.x = item.kernel.annotationFrame.x*(1/page.scale);
    //            item.y = item.kernel.annotationFrame.y*(1/page.scale);
    //            item.width = item.kernel.annotationFrame.width*(1/page.scale);
    //            item.height = item.kernel.annotationFrame.height*(1/page.scale);
    //        }
    //    }

    property var currentUserAnnotationsCount : 0

    property var currentActiveAnnotation

    property bool isActive : false

    height: page.pageHeight*documentController.documentScale + page.inset*2

    function grabWithImage(image)
    {
        page.grabToImage(function(result) {
            image.source = result.url;
        }, Qt.size(200, 300));
    }

    Timer {
        id: readyTimer
        interval: 1
        running: false
        repeat: false
        onTriggered: {
            page.pageReady();
        }
    }

    Rectangle {
        id: pageView

        scale: 1/documentController.documentScale

        width: page.pageWidth*documentController.documentScale
        height: page.pageHeight*documentController.documentScale
        anchors.verticalCenter: page.verticalCenter
        anchors.horizontalCenter: page.horizontalCenter


//            width: page.pageWidth*documentController.documentScale
//            height: page.pageHeight*documentController.documentScale
//            anchors.verticalCenter: page.verticalCenter
//            anchors.horizontalCenter: page.horizontalCenter

        KSImageDrawer {
            id: pageImage
            anchors.fill: parent
            //            source: page.isActive ? "image://page_preview/%1".arg(page.pageIndex) : ""
            //            cache: false
            //            sourceSize.width: page.pageWidth*page.scale
            //            sourceSize.height: page.pageHeight*page.scale
        }

        border.color: "#aa000000"
        border.width: 1

        layer.enabled: true
        layer.effect: DropShadow {
            color: "#aa000000"
            radius: 8
            samples: 32
            horizontalOffset: 4
            verticalOffset: 5
            spread: 0
        }
    }

    Item {

        id: annotatioView

        width: page.pageWidth
        height: page.pageHeight
        anchors.verticalCenter: page.verticalCenter
        anchors.horizontalCenter: page.horizontalCenter

        Repeater {
            id: annotationRepeater
            model: page.annotationsModel

//            width: page.pageWidth
//            height: page.pageHeight
//            anchors.verticalCenter: page.verticalCenter
//            anchors.horizontalCenter: page.horizontalCenter

            property var loadedAnnotationsCount: 0
            property bool isLoaded : false

            Component.onCompleted: {
                annotationRepeater.isLoaded = Qt.binding(function() {
                    return annotationRepeater.count == annotationRepeater.loadedAnnotationsCount;
                })
            }

            onIsLoadedChanged: {
                if(isLoaded)
                {
                    readyTimer.start();
                }
            }

            onItemAdded: {
                if(page.annotationsModel.get(index).annotationParticipant == "Participant 1")
                {
                    page.currentUserAnnotationsCount = page.currentUserAnnotationsCount + 1
                }
            }

            onItemRemoved: {
                loadedAnnotationsCount = loadedAnnotationsCount - 1;
                if(page.annotationsModel.get(index).annotationParticipant == "Participant 1")
                {
                    page.currentUserAnnotationsCount = page.currentUserAnnotationsCount - 1
                }
                readyTimer.start();
            }

            delegate:
                Loader {
                id: annotationLoader
                active: page.isActive
                source: "KSFrameItem.qml"//qmlFile
                onLoaded: {
                    item.annotationType = annotationType;
                    item.annotationParticipant = annotationParticipant;
                    switch(item.annotationType)
                    {
                    case KSAnnotationItem.AnnotationText:
                        item.kernel = Qt.createQmlObject(
                                    'import QtQuick 2.0; import com.keepsolid.app.paintview 1.0; KSTextAnnotation{id:annot; margin: 1; type:KSAnnotationItem.AnnotationText; anchors.fill: parent; z:1}',
                                    item,
                                    'TextAnnotation');
                        //                        item.kernel.innerScale = Qt.binding(function(){
                        //                            return page.scale;//(1/page.scale);
                        //                        });

                        break;
                    case KSAnnotationItem.AnnotationDate:
                        item.kernel = Qt.createQmlObject(
                                    'import QtQuick 2.0;import com.keepsolid.app.paintview 1.0; KSDateAnnotation{id:annot; margin: 1; type:KSAnnotationItem.AnnotationDate; anchors.fill: parent; z:1}',
                                    item,
                                    'DateAnnotation');
                        //                        item.kernel.innerScale = Qt.binding(function(){
                        //                            return page.scale;//(1/page.scale);
                        //                        });
                        break;
                    case KSAnnotationItem.AnnotationSignature:
                        item.kernel = Qt.createQmlObject(
                                    'import QtQuick 2.0;import com.keepsolid.app.paintview 1.0; KSImageAnnotation{id:annot; margin: 1; type:KSAnnotationItem.AnnotationSignature; anchors.fill: parent; z:1}',
                                    item,
                                    'ImageAnnotation');

                        break;
                    case KSAnnotationItem.AnnotationInitials:
                        item.kernel = Qt.createQmlObject(
                                    'import QtQuick 2.0;import com.keepsolid.app.paintview 1.0; KSImageAnnotation{id:annot; margin: 1; type:KSAnnotationItem.AnnotationInitials; anchors.fill: parent; z:1}',
                                    item,
                                    'ImageAnnotation');
                        break;
                    }


                    //                    item.x = item.kernel.annotationFrame.x*(1/page.scale);
                    //                    item.y = item.kernel.annotationFrame.y*(1/page.scale);
                    //                    item.width = item.kernel.annotationFrame.width*(1/page.scale);
                    //                    item.height = item.kernel.annotationFrame.height*(1/page.scale);




                    item.x = Math.random()*(page.pageWidth - width);
                    item.y = Math.random()*(page.pageHeight - height);

                    annotationRepeater.loadedAnnotationsCount = annotationRepeater.loadedAnnotationsCount + 1;

                    console.log("annotations loaded: ",annotationRepeater.loadedAnnotationsCount);
                }

                Binding {
                    target: item
                    property: "isActive"
                    value: false
                    when: item != page.currentActiveAnnotation
                }

                Binding {
                    target: item
                    property: "isActive"
                    value: true
                    when: item == page.currentActiveAnnotation
                }

                Connections {
                    target: item
                    onReleased:
                    {
                        annotationDidRelease(item);
                        readyTimer.start();
                    }
                }

                Connections {
                    target: item
                    onPressed:
                    {
                        annotationDidPress(item);
                    }
                }

                Connections {
                    target: item
                    onRightClick: {

                        console.log("* onRightClick")
                    }
                }
            }
        }

    }
}


