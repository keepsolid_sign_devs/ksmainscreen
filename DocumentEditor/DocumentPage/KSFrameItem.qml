import QtQuick 2.0
import com.keepsolid.app.paintview 1.0
import "../../ui/" as KSControls
import Theme 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: frameItem
    property var kernel : null
    property color annotationColor : Theme.authorAnnotationColor
    property bool isActive : false
    property var borderWidth : annoBorder.borderWidth

    property var annotationType: KSAnnotationItem.AnnotationText
    property var annotationParticipant: ""

    color: annotationColor

    signal rightClick();
    signal doubleClick();
    signal pressed();
    signal released();

    KSAnnotationBorder
    {
        id: annoBorder
        z: 2
        color: frameItem.annotationColor
        anchors.fill: parent
        isActive: frameItem.isActive
    }

    Component.onCompleted: {
        var c = frameItem.annotationColor
        c.a = 0.35
        frameItem.color = c
    }

    MouseArea {
        id: frameItemMouseArea
        z: 2
        anchors.fill: frameItem
        drag.target: frameItem
        drag.axis: Drag.XAndYAxis
        drag.minimumX: 0
        drag.minimumY: 0
        drag.maximumX: page.pageWidth - width
        drag.maximumY: page.pageHeight - height
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button == Qt.RightButton)
            {
                frameItem.rightClick();
            }
        }

        onDoubleClicked: {
            if(frameItem.kernel.indicator)
            {
                frameItem.kernel.indicator.running = true;
            }

            frameItem.doubleClick();
            frameItem.kernel.kernelData = "image://myprovider/200/200?image=" + Math.floor(Math.random() * (1000 - 0) + 0);
        }
        onPressed: {
//            frameItem.isActive = true;
            frameItem.pressed();
        }
        onReleased: {
            frameItem.kernel.updateKernelRect()
            frameItem.released();
        }
    }

    Repeater {
        model: 4
        delegate: KSControls.ResizableDot {
            z: 3
            id: resizeDot
            clockWise: index
            visible: frameItem.isActive
            width: 10
            height: 10

            anchors.rightMargin: -(width)*0.5
            anchors.bottomMargin: - (height)*0.5
            anchors.leftMargin: -(width)*0.5
            anchors.topMargin: - (height)*0.5

            dragWidth:page.pageWidth - anchors.rightMargin - width*0.5
            dragHeight:page.pageHeight - anchors.bottomMargin - height*0.5

            onIsActiveChanged: {
                if(resizeDot.isActive == false)
                {
                    frameItem.kernel.updateKernelRect();
                    frameItem.released();
                }
                else
                {
                    frameItem.pressed();
                }
            }
        }
    }


    x: Math.random()*(page.pageWidth - width);
    y: Math.random()*(page.pageHeight - height)
    width: 100
    height: 100

}
