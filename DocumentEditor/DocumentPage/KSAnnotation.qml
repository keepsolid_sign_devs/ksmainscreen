import QtQuick 2.7
import Theme 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import com.keepsolid.app.paintview 1.0
import "../../ui/" as KSControls


KSAnnotationItem
{
    id: annot

    property alias indicator: indicator
    property int borderWidth : 1
    signal rightClick();
    signal doubleClick();
    signal pressed();
    signal released();
    property Component kernelPresenter : null

    onKernelPresenterChanged: {
        annot.kernelPresenter
    }

    BusyIndicator {
        z: 1
        id: indicator
        anchors.centerIn: parent
        running: false
    }

    MouseArea {
        id: annotationMouseArea
        z: 1
        anchors.fill: parent
        drag.target: parent
        drag.axis: Drag.XAndYAxis
        drag.minimumX: 0
        drag.minimumY: 0
        drag.maximumX: page.pageWidth - width
        drag.maximumY: page.pageHeight - height
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button == Qt.RightButton)
            {
                annot.rightClick();
            }
        }

        onDoubleClicked: {
            indicator.running = true;
            annot.doubleClick();

        }
        onPressed: {
            annot.pressed();
        }
        onReleased: {
            annot.target.updateKernelRect()
            annot.released();
        }
    }

    KSControls.ResizableDot {
        z: 2
        id: resizeDot
        width: 15
        height: 15
        anchors.right: parent.right
        anchors.rightMargin: -(width + borderWidth)*0.5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: - (height + borderWidth)*0.5
        dragWidth:page.pageWidth - anchors.rightMargin - width*0.5
        dragHeight:page.pageHeight - anchors.bottomMargin - height*0.5

        onIsActiveChanged: {
            if(resizeDot.isActive == false)
            {
                annot.target.updateKernelRect();
                annot.released();
            }
            else
            {
                annot.pressed();
            }
        }
    }

    x: Math.random()*(page.pageWidth - width);
    y: Math.random()*(page.pageHeight - height)
    width: 100
    height: 100
}
