import QtQuick 2.0
import com.keepsolid.app.paintview 1.0
import QtQuick.Controls 2.0

KSAnnotationItem {
    id:root
    property alias indicator: indicator

    BusyIndicator {
        z: 2
        id: indicator
        anchors.centerIn: parent
        running: false
    }

    Image {
        id: img
        z: 0
        anchors.top: parent.top
        anchors.topMargin: 0 + root.margin
        anchors.right: parent.right
        anchors.rightMargin: 0 + root.margin
        anchors.left: parent.left
        anchors.leftMargin: 0 + root.margin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0 + root.margin

        source: root.kernelData
        onStatusChanged: {
            if(status == Image.Ready)
            {
                indicator.running = false;
            }
            else
            {
                indicator.running = true;
            }
        }
    }

}
