import QtQuick 2.0
import com.keepsolid.app.paintview 1.0

KSAnnotationText {
    id:root

    property var fontFamily : "Arial"

    property var indicator: null

    Text {
        text: root.kernelData

        anchors.top: parent.top
        anchors.topMargin: 2 + root.margin
        anchors.right: parent.right
        anchors.rightMargin: 5 + root.margin
        anchors.left: parent.left
        anchors.leftMargin: 5 + root.margin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0 + root.margin

        font.bold: root.isBold
        font.italic: root.isItalic
        font.family: root.fontFamily
        font.pointSize: root.fontSize

        renderType: Text.NativeRendering
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
}
