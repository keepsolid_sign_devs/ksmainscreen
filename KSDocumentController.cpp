#include "KSDocumentController.h"
#include <QPdfDocument>
#include "kspageitem.h"
#include "KSPageRenderSession.h"

KSDocumentController::KSDocumentController(QQuickItem *parent)
    : QQuickItem(parent)
    , m_pdf(new QPdfDocument(this))
    , m_documentHeight(0)
    , m_documentScale(1.0)
{
    connect(m_pdf, &QPdfDocument::statusChanged, this, &KSDocumentController::documentStatusChanged);

    connect(KSPageRenderSession::instance(),SIGNAL(imageReady(int, const QImage&)),SIGNAL(imageChanged(int, const QImage&)));
}

void KSDocumentController::setDocId(QString docId)
{
    if (m_docId == docId)
        return;

    QFileInfo file("C:\\prj\\ksmainscreen\\pdf.pdf");//\Eloquent_JavaScript.pdf");//\icon_sigin_white.pdf");//\pdf-test.pdf");//

    QPdfDocument::DocumentError error = m_pdf->load(file.absoluteFilePath());
    _imageCache.clear();

    if(QPdfDocument::NoError != error)
    {
        qDebug() << "PDF loading error: " << file.absoluteFilePath();
    }
    m_docId = docId;
    emit docIdChanged(m_docId);
}

void KSDocumentController::documentStatusChanged()
{
    if (m_pdf->status() == QPdfDocument::Ready) {

        KSPageRenderSession::instance()->setDocument(m_pdf);

        qreal height = 0;
        for(int i = 0; i < m_pdf->pageCount(); i++)
        {
            height += pageSize(i).height();
        }

        setDocumentHeight(height);
    }
}

void KSDocumentController::setPageActivationIndex(int pageActivationIndex)
{
    KSPageRenderSession::instance()->addThread(pageActivationIndex,documentScale());

}

QSizeF KSDocumentController::pageSize(int pageIndex)
{
    if (m_pdf->status() == QPdfDocument::Ready)
    {
        return m_pdf->pageSize(pageIndex);
    }
    else
    {
        return QSize();
    }
}

QImage *KSDocumentController::readPageImage(int pageIndex, QSize requestedSize)
{
    if (m_pdf->status() != QPdfDocument::Ready)
    {
        return nullptr;
    }
    if(_imageCache.contains(pageIndex)
            && _imageCache[pageIndex]->size().width() > requestedSize.width()
            && _imageCache[pageIndex]->size().height() > requestedSize.height())
    {
        return _imageCache[pageIndex];
    }

    QElapsedTimer timer;
    timer.start();

    QImage *img = new QImage(m_pdf->render(pageIndex, requestedSize));

    _imageCache.insert(pageIndex, img);
    const qreal secs = timer.nsecsElapsed() / 1000000000.0;

    qDebug() << "page" << pageIndex << "size" << requestedSize << "in" << secs <<
                "secs";

    return img;
}
