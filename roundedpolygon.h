#ifndef ROUNDEDPOLYGON_H
#define ROUNDEDPOLYGON_H

#include <QtQuick>

class RoundedPolygon : public QPolygon
{
public:
    RoundedPolygon() : m_isClosePath(false)
    {    SetRadius(4); }
    void SetRadius(unsigned int iRadius)
    {    m_uiRadius = iRadius; }
    const QPainterPath& GetPath();
    const QPainterPath& GetNarrowPath();
    const QPainterPath& GetClosedPath();
    const QPainterPath& GetVectorPath();
    bool isClosePath() { return m_isClosePath; };
    void setClosePath(bool isClosePath) { m_isClosePath = isClosePath; };
private:
    QPointF GetLineStart(int i) const;
    QPointF GetLineEnd(int i) const;
    float GetDistance(QPointF pt1, QPointF pt2) const;
private:
    QPainterPath m_path;
    QPainterPath m_narrowPath;
    QPainterPath m_vectorPath;
    unsigned int m_uiRadius;
    bool m_isClosePath;
};

#endif // ROUNDEDPOLYGON_H
