#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QObject>
#include "KSDocumentController.h"

class AppController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int elevation READ elevation WRITE setElevation NOTIFY elevationChanged)
    Q_PROPERTY(KSDocumentController * docController READ docController WRITE setDocController NOTIFY docControllerChanged)

public:
    int elevation() const;
    void setElevation(int elevation);

    AppController();
    Q_INVOKABLE void lock();

    static AppController *instance()
    {
        static AppController *appController = new AppController();
        return appController;
    }

    KSDocumentController * docController() const
    {
        return m_docController;
    }

signals:
    void elevationChanged();
    void docControllerChanged(KSDocumentController * docController);

public slots:

void setDocController(KSDocumentController * docController)
{
    if (m_docController == docController)
        return;

    m_docController = docController;
    emit docControllerChanged(m_docController);
}

private:
int m_elevation;
KSDocumentController * m_docController;
};

#endif // APPCONTROLLER_H
