#ifndef KSUISTYLE_H
#define KSUISTYLE_H


#include <QObject>
#include <QtQml>

class KSUIStyle : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int elevation READ elevation WRITE setElevation NOTIFY elevationChanged)

public:
    explicit KSUIStyle(QObject *parent = nullptr);

    int elevation() const;
    void setElevation(int elevation);

signals:
    void elevationChanged();

private:
    int m_elevation;
};

#endif // KSUISTYLE_H
