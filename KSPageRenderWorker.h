#ifndef KSPAGERENDERWORKER_H
#define KSPAGERENDERWORKER_H

#include <QObject>
#include "pagerenderer.h"
#include <QPdfDocument>

class KSPageRenderWorker : public QObject
{
    Q_OBJECT
public:
    explicit KSPageRenderWorker(QObject *parent = nullptr, int pageIndex = 0, qreal zoom = 1.0);
    ~KSPageRenderWorker();

    void setDocument(QPdfDocument *document) { m_pdf = document; };

public slots:
    void process();
    void stop();

signals:
    void finished();
    void pageReady(int page, qreal zoom, const QImage image);

private:
    qreal m_zoom;
    int m_pageIndex;
    QPdfDocument * m_pdf;
    int m_totalPagesRendered;
    qreal m_totalRenderTime;

};

#endif // KSPAGERENDERWORKER_H
