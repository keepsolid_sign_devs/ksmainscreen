#include "KSPageRenderWorker.h"
#include <QPdfDocument>
#include <QElapsedTimer>
#include <QDebug>

KSPageRenderWorker::KSPageRenderWorker(QObject *parent, int pageIndex, qreal zoom)
    : QObject(parent)
    , m_pdf(NULL)
    , m_pageIndex(pageIndex)
    , m_zoom(zoom)
{

}

KSPageRenderWorker::~KSPageRenderWorker ()
{
//    if (_render != NULL) {
//        delete _render;
//    }
}

void KSPageRenderWorker::process()
{
    if(m_pdf == NULL || m_pdf->status() != QPdfDocument::Ready)
    {
        emit finished();
        return;
    }

    const QSizeF size = m_pdf->pageSize(m_pageIndex) * m_zoom;

    QElapsedTimer timer;
    timer.start();

    const QImage &img = m_pdf->render(m_pageIndex, size.toSize());

    const qreal secs = timer.nsecsElapsed() / 1000000000.0;

    m_totalRenderTime += secs;
    ++m_totalPagesRendered;

    qDebug() << "page" << m_pageIndex << "zoom" << m_zoom << "size" << size << "in" << secs <<
                "secs;" <<
                "avg" << m_totalRenderTime / m_totalPagesRendered;

    emit pageReady(m_pageIndex, m_zoom, img);
    emit finished();
}

void KSPageRenderWorker::stop() {

    return ;
}
