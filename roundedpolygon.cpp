#include "roundedpolygon.h"


float RoundedPolygon::GetDistance(QPointF pt1, QPointF pt2) const
{
    float fD = (pt1.x() - pt2.x())*(pt1.x() - pt2.x()) +
            (pt1.y() - pt2.y()) * (pt1.y() - pt2.y());
    return sqrtf(fD);
}


QPointF RoundedPolygon::GetLineStart(int i) const
{
    QPointF pt;
    QPoint pt1 = at(i);
    QPoint pt2 = at((i+1) % count());
    qreal fRat = 0.5;//m_uiRadius / GetDistance(pt1, pt2);
    if (fRat > 0.5f)
        fRat = 0.5f;

    pt.setX((1.0f-fRat)*pt1.x() + fRat*pt2.x());
    pt.setY((1.0f-fRat)*pt1.y() + fRat*pt2.y());
    return pt;
}


QPointF RoundedPolygon::GetLineEnd(int i) const
{
    QPointF pt;
    QPoint pt1 = at(i);
    QPoint pt2 = at((i+1) % count());
    qreal fRat = 0.5;//m_uiRadius / GetDistance(pt1, pt2);
    if (fRat > 0.5f)
        fRat = 0.5f;
    pt.setX(fRat*pt1.x() + (1.0f - fRat)*pt2.x());
    pt.setY(fRat*pt1.y() + (1.0f - fRat)*pt2.y());
    return pt;
}

const QPainterPath& RoundedPolygon::GetClosedPath()
{
    GetPath();
    // close the last corner
    QPointF pt1 = GetLineStart(0);
    m_path.quadTo(at(0), pt1);

    return m_path;
}

const QPainterPath& RoundedPolygon::GetVectorPath()
{
    m_vectorPath = QPainterPath();

    if (count() < 3) {
        qWarning() << "Polygon should have at least 3 points!";
        return m_vectorPath;
    }
    QPointF pt1;
    QPointF pt2;
    m_vectorPath.moveTo(at(0).x(),at(0).y());
    for (int i = 1; i < count() - 1; i++)
    {


        pt1 = GetLineStart(i);
        pt2 = GetLineEnd(i);
        m_vectorPath.lineTo(at(i).x(),at(i).y());

    }

    return m_vectorPath;
}

const QPainterPath& RoundedPolygon::GetNarrowPath()
{

    m_narrowPath = QPainterPath();

    if (count() < 3) {
        qWarning() << "Polygon should have at least 3 points!";
        return m_narrowPath;
    }

    int radiusX = 5;
    int radiusY = 0;
    QPointF pt1;
    QPointF pt2;
    QPointF firstPoint;

    translate(+radiusX,radiusY);
    firstPoint = at(0);
//    m_narrowPath.moveTo(firstPoint);

    for (int i = 0; i < count() - 1; i++) {
        pt1 = GetLineStart(i);
        pt2 = GetLineEnd(i);

        if (i == 0)
        {
            firstPoint = pt1;
            m_narrowPath.moveTo(pt1);
        }
        else
        {
            m_narrowPath.quadTo(at(i), pt1);
        }

        m_narrowPath.lineTo(pt2);
    }

    if(m_isClosePath)
    {
        m_narrowPath.lineTo(at( count() - 1));
    }


    translate(-radiusX,-radiusY);
    translate(-radiusX,-radiusY);


    if(m_isClosePath)
    {
        m_narrowPath.lineTo(at( count() - 1));
    }

    for (int i = count() - 1; i > 0; i--) {
        pt1 = GetLineEnd(i - 1);
        pt2 = GetLineStart(i - 1);

        if (i ==  count() - 1)
            m_narrowPath.lineTo(pt1);
        else
        {
            m_narrowPath.quadTo(at(i), pt1);
        }

        m_narrowPath.lineTo(pt2);
    }

//    m_narrowPath.lineTo(GetLineStart(0));


    translate(+radiusX,radiusY);
    translate(+radiusX,radiusY);
    m_narrowPath.lineTo(firstPoint);
    translate(-radiusX,-radiusY);



    //    //++++++++++++++++++


    //    translate(radiusX,radiusY);

    //    firstPoint = GetLineStart(0);


    //    for (int i = 0; i < count() - 1; i++) {
    //        pt1 = GetLineStart(i);
    //        pt2 = GetLineEnd(i);

    //        if (i == 0)
    //            m_narrowPath.moveTo(pt1);
    //        else
    //        {
    //            m_narrowPath.quadTo(at(i), pt1);
    //        }

    //        m_narrowPath.lineTo(pt2);
    //    }

    //    translate(-radiusX,-radiusY);
    //    translate(-radiusX,-radiusY);

    //    for (int i =  count() - 1; i >= 1; i--) {
    //        pt1 = GetLineEnd(i - 1);
    //        pt2 = GetLineStart(i - 1);

    //        if (i ==  count() - 1)
    //            m_narrowPath.lineTo(pt1);
    //        else
    //        {
    //            m_narrowPath.quadTo(at(i), pt1);
    //        }

    //        m_narrowPath.lineTo(pt2);
    //    }

    //    translate(radiusX,radiusY);

    //


    return m_narrowPath;
}

const QPainterPath& RoundedPolygon::GetPath()
{
    m_path = QPainterPath();

    if (count() < 3) {
        qWarning() << "Polygon should have at least 3 points!";
        return m_path;
    }

    QPointF pt1;
    QPointF pt2;
    for (int i = 0; i < count() - 1; i++) {
        pt1 = GetLineStart(i);
        pt2 = GetLineEnd(i);

        if (i == 0)
            m_path.moveTo(pt1);
        else
        {
            m_path.quadTo(at(i), pt1);
        }

        m_path.lineTo(pt2);
    }

    return m_path;
}
