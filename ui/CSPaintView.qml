import QtQuick 2.0
import QtQuick.Controls 2.0
import com.keepsolid.app.paintview 1.0
import Theme 1.0
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: view

    property var painter: qsTr("My Signature")
    property var source: paintView.source
    property var implicitWidth: paintView.implicitWidth + painterBounds.anchors.leftMargin + painterBounds.anchors.rightMargin
    property var implicitHeight: paintView.implicitHeight
    property var textureWidth: paintView.textureWidth
    property var textureHeight: paintView.textureHeight
    property var scale: 1


    function importFile(file)
    {
        paintView.importFile(file);
    }

    onSourceChanged: {
        console.log("onSourceChanged: ", view.source);
    }

    ListModel {
        id: tools
        ListElement {
            icon: "../resources/signatureToolsBar/iconDrawSignPencilNormal.png"
            highlightedIcon: "../resources/signatureToolsBar/iconDrawSignPencilActive.png"
        }
        ListElement {
            icon: "../resources/signatureToolsBar/iconDrawSignBrushNormal.png"
            highlightedIcon: "../resources/signatureToolsBar/iconDrawSignBrushActive.png"
        }
        ListElement {
            icon: "../resources/signatureToolsBar/iconDrawSignEraseNormal.png"
            highlightedIcon: "../resources/signatureToolsBar/iconDrawSignEraseActive.png"
        }
    }

    color: "#00000000"
    border.color: Theme.whiteThree

    function save()
    {
        paintView.save();

    }

    Rectangle {
        id: rectangle
        x: 160
        y: -10
        width: 135
        height: 19
        color: "#ffffff"
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: text1
            y: 0
            text: painter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pixelSize: 12
            color: Theme.warmGreyColor
        }
    }

    Rectangle {
        id: painterBounds
        color: "#00000000"
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 63

        KSPaintView {
            id: paintView
            scale: view.scale

            anchors.fill: parent

            onTextureWidthChanged: {
                 console.log("textureWidth: ",item.textureWidth);

            }

        }

        Rectangle {
            id: separator
            height: 2
            color: Theme.whiteThree
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 0
        }

        MouseArea {
            id: mouseArea
            y: 2
            anchors.fill: parent
            onPressed:{
                paintView.startPoint(Qt.point(mouse.x, mouse.y))
            }
            onPositionChanged: {
                paintView.nextPoint(Qt.point(mouse.x, mouse.y))
            }
            onReleased:  {
                paintView.endPoint(Qt.point(mouse.x, mouse.y))
            }
        }
    }

    Text {
        id: text2
        x: 400
        y: 42
        width: 47
        height: 15
        text: qsTr("Clear")
        horizontalAlignment: Text.AlignRight
        anchors.right: parent.right
        anchors.rightMargin: 8
        font.pixelSize: 12
        color:Theme.blueColor

        MouseArea {
            id: mouseArea1
            anchors.fill: parent
            onClicked: paintView.clear()
        }
    }

    Text {
        id: text3
        x: 347
        y: 42
        width: 47
        height: 15
        color: Theme.blueColor
        text: qsTr("Save")
        MouseArea {
            id: mouseArea2
            anchors.fill: parent
            onClicked: {
                paintView.save()
            }
        }
        horizontalAlignment: Text.AlignRight
        anchors.right: parent.right
        anchors.rightMargin: 61
        font.pixelSize: 12
    }

    ToolBar {
        id: toolBar
        x: 1
        y: 42
        width: 334
        height: 28
        background: Rectangle {
            color:"transparent"
            anchors.fill: parent
        }
        anchors.bottom: painterBounds.top
        anchors.bottomMargin: 6
        anchors.left: parent.left
        anchors.leftMargin: 23

        Row {
            id: toolsRow
            anchors.fill: parent
            spacing: 5

            Repeater {
                id: drawTools
                model: tools
                delegate: ToolButton {
                    id: toolButton
                    width: toolBar.height
                    height: toolBar.height
                    background: Rectangle {
                        color: "transparent"//paintView.brushStyle == index ? Theme.greyishBrownColor :
                    }

                    contentItem: Image {

                        source: paintView.brushStyle == index || toolButton.hovered ? highlightedIcon : icon
                    }


                    onClicked: {
                        paintView.brushStyle = index;
                    }
                }
            }

            ComboBox {
                id : btnColorCombo
                width: toolBar.height
                height: toolBar.height

                contentItem: ItemDelegate {
                    width: toolBar.height
                    height: toolBar.height
                    padding: 10
                    contentItem: Rectangle {
                        id: btnColorRect
                        color: paintView.color
                        radius: btnColorRect.width*0.5
                    }

                    onClicked: {
                        btnColorCombo.popup.open()
                    }
                }

                background: Rectangle {
                    color:"transparent"
                    anchors.fill: parent
                }

                model : ["#000000", "#e41e11","#5c2b85", "#2370b3", "#5c2b85", Theme.whiteThree]
                indicator: Rectangle {}

                delegate: ItemDelegate {
                    width: toolBar.height
                    height: toolBar.height
                    padding: 5
                    contentItem: Rectangle {
                        id: btnColorItem
                        color: modelData
                        radius: btnColorItem.width*0.5
                    }

                    onClicked: {
                        paintView.color = modelData
                    }
                }
            }
        }
    }

}
