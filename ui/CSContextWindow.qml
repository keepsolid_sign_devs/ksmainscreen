import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import QtQuick.Controls.Styles 1.4

Window {
    id: dropdown
    property bool isOpenned : false
    property var borderWidth: 2
    property color borderColor: "#00ff00"
    property color groundColor: "#3c3c3c"
    visible: dropdown.opacity != 0
    height: 32
    width: 64
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint

    property var cornerHeight: 8
    property bool topCorner: false

    Canvas {
        id: ground
        anchors.fill: parent

        onPaint: {
            var ctx = getContext("2d");

            ctx.fillStyle = groundColor;
            if(!topCorner)
            {
                ctx.beginPath();
                ctx.moveTo(0,0);
                ctx.lineTo(ground.width,0);
                ctx.lineTo(ground.width,ground.height - cornerHeight);

                ctx.lineTo(ground.width*0.5 + cornerHeight,ground.height - cornerHeight);
                ctx.lineTo(ground.width*0.5, ground.height);
                ctx.lineTo(ground.width*0.5 - cornerHeight,ground.height - cornerHeight);

                ctx.lineTo(0,ground.height - cornerHeight);

                ctx.closePath();

                ctx.fillStyle = groundColor;
                ctx.fill();
                ctx.lineWidth = borderWidth;
                ctx.strokeStyle = borderColor;
                ctx.stroke();
            }
            else
            {
                ctx.beginPath();
                ctx.moveTo(0,cornerHeight);

                ctx.lineTo(ground.width*0.5 - cornerHeight, cornerHeight);
                ctx.lineTo(ground.width*0.5, 0);
                ctx.lineTo(ground.width*0.5 + cornerHeight, cornerHeight);

                ctx.lineTo(ground.width, cornerHeight);
                ctx.lineTo(ground.width, ground.height - cornerHeight);
                ctx.lineTo(0,ground.height - cornerHeight);

                ctx.closePath();

                ctx.fillStyle = groundColor;
                ctx.fill();
                ctx.lineWidth = borderWidth;
                ctx.strokeStyle = borderColor;
                ctx.stroke();
            }
        }
    }

    Component.onCompleted: {
        opacityAnimation.enabled = false;
        dropdown.opacity = 0;
    }

    onWidthChanged: {
        updateFrame();
    }

    onActiveChanged: {
        if (!active) {
            hide(true);
        }
    }

    Timer {
        id: openTimer
        interval: 600
        running: false
        repeat: false
        onTriggered: dropdown.show(true)
    }
    Timer {
        id: layoutTimer
        interval: 2
        running: false
        repeat: false
        onTriggered: dropdown.updateFrame()
    }

    onIsOpennedChanged: {
        dropdown.opacity = (isOpenned ? 1 : 0);
    }

    Behavior on opacity {
        id: opacityAnimation
        NumberAnimation { duration: 100 }
    }

    function updateFrame()//will be override
    {
        updatePosition(0,0);
    }

    function updatePosition(x,y)
    {
        if(y < mainWindow.y || y > mainWindow.y + mainWindow.height)
        {
            hide(true);
        }
        else
        {
            show(true);
        }

        dropdown.x = x;
        dropdown.y = y;
    }

    function updateFrameDelayed()
    {
        layoutTimer.start();
    }

    function showDelayed()
    {
        openTimer.start();
    }

    function show(a)
    {
        if(dropdown.isOpenned)
        {
            return;
        }

        openTimer.stop();
        opacityAnimation.enabled = a;

        dropdown.isOpenned = true;
    }

    function isMenuOpenned()
    {
        return isOpenned;
    }

    function hide(a)
    {
        if(!dropdown.isOpenned)
        {
            return;
        }
        openTimer.stop();
        opacityAnimation.enabled = a;
        dropdown.isOpenned = false;
    }
}
