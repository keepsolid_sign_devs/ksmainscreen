import QtQuick 2.7
import Theme 1.0

Rectangle {
    id: resizableDot
    color: Theme.whiteColor
    property bool isActive: false
    property int minimumWidth: 50
    property int minimumHeight: 50
    property double dragWidth: 0
    property double dragHeight: 0
    property int clockWise : 3
    radius: resizableDot.width*0.5
    state: clockWise == 0 ? "LeftBottom" : clockWise == 1 ? "LeftTop" : clockWise == 2 ? "RightTop" : "RightBottom"
    property bool isCustomResize : false

    states: [
        State {
            name: "LeftBottom"

            AnchorChanges {
                target: resizableDot
                anchors.left: resizableDot.parent.left
                anchors.bottom: resizableDot.parent.bottom
            }
        },

        State {
            name: "LeftTop"

            AnchorChanges {
                target: resizableDot
                anchors.left: resizableDot.parent.left
                anchors.top: resizableDot.parent.top
            }
        },

        State {
            name: "RightTop"

            AnchorChanges {
                target: resizableDot
                anchors.right: resizableDot.parent.right
                anchors.top: resizableDot.parent.top
            }
        },

        State {
            name: "RightBottom"

            AnchorChanges {
                target: resizableDot
                anchors.right: resizableDot.parent.right
                anchors.bottom: resizableDot.parent.bottom
            }
        }
    ]

    border.color: isActive ? Theme.blueColorAlph5 : Theme.warmGreyColor

    MouseArea {
        id: resizableDotArea
        property int mouseYOffset : 0;
        property int mouseXOffset : 0;
        anchors.fill: parent
        drag{
            target: parent;
            axis: Drag.XAndYAxis
        }
        onPressed: {
            resizableDot.isActive = true;
            mouseYOffset = mouseY;
            mouseXOffset = mouseX;
        }

        onReleased: {
            resizableDot.isActive = false;
        }

        function heightChangeValid(mouseY, mouseYOffset)
        {
            return (resizableDot.dragHeight > resizableDot.parent.y + resizableDot.parent.height + mouseY - mouseYOffset);
        }

        function widthChangeValid(mouseX, mouseXOffset)
        {
            return (resizableDot.dragWidth > resizableDot.parent.x + resizableDot.parent.width + mouseX - mouseXOffset);
        }

        onPositionChanged: {
            var deltaY = mouseY - mouseYOffset
            var deltaX = mouseX - mouseXOffset

            if(resizableDot.isCustomResize == false)
            {
                var deltaMax = Math.max(Math.abs(deltaY),Math.abs(deltaX));
                deltaX = deltaX < 0 ? -deltaMax : deltaMax
                deltaY = deltaY < 0 ? -deltaMax : deltaMax
            }

            switch(resizableDot.clockWise)
            {
            case 1:
                if(heightChangeValid(mouseY,mouseYOffset))
                {
                    resizableDot.parent.height -= deltaY
                    resizableDot.parent.y += deltaY
                    if(resizableDot.parent.height < minimumHeight)
                        resizableDot.parent.height = minimumHeight;
                }
                else
                {
                    resizableDot.parent.height = resizableDot.dragHeight - resizableDot.parent.y;
                }
                break;
            case 2:
                if(heightChangeValid(mouseY,mouseYOffset))
                {
                    resizableDot.parent.height -= deltaY
                    resizableDot.parent.y += deltaY
                    if(resizableDot.parent.height < minimumHeight)
                        resizableDot.parent.height = minimumHeight;
                }
                else
                {
                    resizableDot.parent.height = resizableDot.dragHeight - resizableDot.parent.y;
                }
                break;
            case 0:
            case 3:
                if(heightChangeValid(mouseY,mouseYOffset))
                {
                    resizableDot.parent.height += deltaY
                    if(resizableDot.parent.height < minimumHeight)
                        resizableDot.parent.height = minimumHeight;
                }
                else
                {
                    resizableDot.parent.height = resizableDot.dragHeight - resizableDot.parent.y;
                }
                break;
            }

            switch(resizableDot.clockWise)
            {
            case 0:
                if(widthChangeValid(mouseX, mouseXOffset))
                {
                    resizableDot.parent.width -= deltaX
                    resizableDot.parent.x += deltaX
                    if(resizableDot.parent.width < minimumWidth)
                        resizableDot.parent.width = minimumWidth
                }
                else
                {
                    resizableDot.parent.width = resizableDot.dragWidth - resizableDot.parent.x;
                }
                break;

            case 1:
                if(widthChangeValid(mouseX, mouseXOffset))
                {
                    resizableDot.parent.width -= deltaX
                    resizableDot.parent.x += deltaX
                    if(resizableDot.parent.width < minimumWidth)
                        resizableDot.parent.width = minimumWidth
                }
                else
                {
                    resizableDot.parent.width = resizableDot.dragWidth - resizableDot.parent.x;
                }
                break;

            case 2:
            case 3:
                if(widthChangeValid(mouseX, mouseXOffset))
                {

                    resizableDot.parent.width += deltaX
                    if(resizableDot.parent.width < minimumWidth)
                        resizableDot.parent.width = minimumWidth
                }
                else
                {
                    resizableDot.parent.width = resizableDot.dragWidth - resizableDot.parent.x;
                }
                break;
            }
        }
    }
}
