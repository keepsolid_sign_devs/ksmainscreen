import QtQuick 2.0
import QtQuick.Window 2.2
//import Qt.labs.calendar 1.0
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1
import Theme 1.0

import QtQuick.Controls 1.2
import QtQuick.Controls.Private 1.0
import QtQuick.Controls.Styles 1.1

CSContextWindow {

    id: editDateMenu
    property var date: new Date(2014, 0, 1)
    property var dateTextFormat: "dd-MM-yyyy"
    color: "transparent"

    groundColor: "#ffffff"
    borderColor: "#dddddd"
    topCorner:true

    width: dateGrid.width + borderWidth*2
    height: dateGrid.height + cornerHeight + borderWidth*6

    onDateChanged: {
        inputDate.text = Qt.formatDate(date, dateTextFormat);
        calendar.selectedDate = date;
    }

    Column {
        id: dateGrid
        y: editDateMenu.cornerHeight + borderWidth
        x: borderWidth

        TextInput {
            id: inputDate
            width: calendar.width
            text: Qt.formatDate(new Date(2014, 0, 1), dateTextFormat)
            font.pointSize: 18
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            inputMask: "00-00-0000"
            inputMethodHints: Qt.ImhDate

            onTextChanged: {
                var newDate = Date.fromLocaleString(Qt.locale(), inputDate.text, dateTextFormat);
                if(newDate != null)
                {
                    editDateMenu.date = newDate
                }
            }
        }

        Calendar {
            id: calendar

            frameVisible: false
            weekNumbersVisible: false
            selectedDate: new Date(2014, 0, 1)
            focus: true

            onSelectedDateChanged: {
                editDateMenu.date = calendar.selectedDate;
            }

            style: CalendarStyle {
                gridColor: "transparent"
                dayDelegate: Item {
                    readonly property color sameMonthDateTextColor: "#444"
                    readonly property color selectedDateColor: Theme.blueColor
                    readonly property color selectedDateTextColor: "white"
                    readonly property color differentMonthDateTextColor: "#bbb"
                    readonly property color invalidDatecolor: "#dddddd"

                    Rectangle {
                        anchors.fill: parent
                        border.color: "transparent"
                        color: styleData.date !== undefined && styleData.selected ? selectedDateColor : "transparent"
                        anchors.margins: styleData.selected ? -1 : 0
                    }

                    Label {
                        id: dayDelegateText
                        text: styleData.date.getDate()
                        anchors.centerIn: parent
                        color: (styleData.valid
                                ? (styleData.selected
                                   ?  selectedDateTextColor
                                   : (styleData.visibleMonth
                                      ? sameMonthDateTextColor
                                      : differentMonthDateTextColor))
                                : invalidDatecolor)
                    }
                }
            }
        }
    }
}
