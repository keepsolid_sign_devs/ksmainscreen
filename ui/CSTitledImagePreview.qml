import QtQuick 2.0
import QtQuick.Controls 2.0
import Theme 1.0
import QtGraphicalEffects 1.0

Rectangle {
    id: preview

    property var source: "null"
    property var titleLabel: label
    property int borderWidth: 10
    property bool isSelected: false
    signal clicked()

    onSourceChanged: {
        console.log("onSourceChanged: ",source);
    }

    width: 240
    height: 260
    color: preview.isSelected ? Theme.blueColor : Theme.whiteThree

    Rectangle {
        id: imageGround
        anchors.right: parent.right
        anchors.rightMargin: preview.borderWidth
        anchors.left: parent.left
        anchors.leftMargin: preview.borderWidth
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        anchors.top: parent.top
        anchors.topMargin: preview.borderWidth
        color: "#ffffffff"
        Image {
            id: image
            anchors.fill: parent
            source: preview.source
            cache: false
        }

        FastBlur {
                anchors.fill: image
                source: image
                radius: image.width*05
            }
    }

    Text {
        id: label
        y: 236
        height: 24
        text: qsTr("Text")
        anchors.right: parent.right
        anchors.rightMargin: preview.borderWidth
        anchors.left: parent.left
        anchors.leftMargin: preview.borderWidth
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        horizontalAlignment: Text.AlignHCenter
        color: preview.isSelected ? "#ffffffff" : Theme.greyishBrownColor
        font.pixelSize: 14
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            preview.clicked()
            console.info("preview clicked!")
        }
    }
}
