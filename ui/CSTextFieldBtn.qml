import QtQuick 2.0
import QtQuick.Controls 2.0
import Theme 1.0

TextField {
    id: textEdit
    width: 389
    height: 28

    property alias rbImage: rightButtonImage

    text: qsTr("Text Edit")
    anchors.bottomMargin: -9
    anchors.fill: parent
    opacity: 1
    font.pixelSize: 14
    layer.enabled: true

    Rectangle {
        id: rightButton
        x: textEdit.width - textEdit.height
        width: textEdit.height
        color: Theme.whiteColor
        anchors.top: parent.top
        anchors.topMargin: textEdit.background.border.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: textEdit.background.border.width
        anchors.right: parent.right
        anchors.rightMargin: textEdit.background.border.width

        Image {
            id: rightButtonImage
            anchors.bottomMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent
            source: "../resources/logoImg.png"
            MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                       console.info("image clicked!")
                    }
                }
        }
    }

    background: Rectangle {
        implicitWidth: textEdit.width
        implicitHeight: textEdit.height
        border.width: textEdit.activeFocus ? 1 : 1
        color: textEdit.enabled ? Theme.whiteColor : Theme.whiteTwoColor
        border.color: textEdit.hovered ? Theme.blueColorAlph5 : textEdit.activeFocus ? Theme.blueColor : (textEdit.enabled ? Theme.whiteTwoColor : "transparent")

    }
}
