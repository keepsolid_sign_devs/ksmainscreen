#ifndef KSIMAGEDRAWER_H
#define KSIMAGEDRAWER_H

#include <QtQuick>

class KSImageDrawer : public QQuickPaintedItem
{
    Q_OBJECT
    QImage m_image;

public:
    Q_PROPERTY(const QImage image READ image WRITE setImage NOTIFY imageChanged)

    KSImageDrawer(QQuickItem *parent = Q_NULLPTR);
    void paint(QPainter *painter);
    const QImage  image() const
    {
        return m_image;
    }
public slots:
    void setImage(const QImage image)
    {
//        if (m_image == image)
//            return;

        m_image = image;
        QRectF bound = boundingRect();

//        setContentsSize(m_image.size());

//        qreal scale = bound.width()/m_image.size().width();
//        setContentsScale(scale);
//        setScale(scale);
//        setTextureSize(m_image.size());

        update();
        emit imageChanged(m_image);
    }
signals:
    void imageChanged(const QImage image);
};

#endif // KSIMAGEDRAWER_H
