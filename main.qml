import QtQuick 2.6
import QtQuick.Window 2.2
import "./DocumentEditor/ui" as KSDocumentUI

Window {
    id: mainWindow
    visible: true
    width: 1600
    height: 1000
    minimumWidth: 460
    minimumHeight: 520
    title: qsTr("KeepSolid Sign")
    objectName: "mainWindow"

//    MainForm {
//        anchors.fill: parent
//    }

    DocumentEditor {
        id: documentEditor
        objectName: "documentEditor"
        width: 740
        height: 520
        docId: "abra cadabra"
        anchors.fill: parent
        Component.onCompleted: {
            appController.docController = documentEditor;
        }
    }

    onXChanged: {
        if(documentEditor.contextMenu != null)
            documentEditor.contextMenu.updateFrame();
    }
    onYChanged: {
        if(documentEditor.contextMenu != null)
            documentEditor.contextMenu.updateFrame();
    }

    KSDocumentUI.CSPaintWindow {
        id: paintWindow
        visible: false
        x: mainWindow.x + (mainWindow.width - paintWindow.width)*0.5
        y: mainWindow.y + (mainWindow.height - paintWindow.height)*0.5
    }

    CSTextEditWindow {
        id: textEditWindow
        visible: false
    }
}
