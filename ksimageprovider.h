#ifndef KSIMAGEPROVIDER_H
#define KSIMAGEPROVIDER_H

#include <QObject>
#include <QColor>
#include <QtQuick>

class KSImageProvider : public QQuickImageProvider
{
public:
    KSImageProvider(ImageType type, Flags flags = 0) :
        QQuickImageProvider(type,flags), manager(new QNetworkAccessManager) {  }
    ~KSImageProvider();

    QImage requestImage(const QString & id, QSize * size, const QSize & requestedSize);

    static KSImageProvider *instance()
    {
        static KSImageProvider *provider = new KSImageProvider(QQmlImageProviderBase::Image);
        return provider;
    }
private:
    QNetworkAccessManager *manager;
};

#endif // KSIMAGEPROVIDER_H
