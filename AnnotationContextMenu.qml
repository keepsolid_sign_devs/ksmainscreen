import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import QtQuick.Controls.Styles 1.4
import com.keepsolid.app.paintview 1.0

CSContextWindow {
    id: dropdown
    height: 40
    width: toolsRow.width
    color: "transparent"

    groundColor: "#3c3c3c"
    borderColor: "#5d5d5d"
    topCorner: false

    signal itemDidClick(var index, var sender)


    property var toolsTitles;

    ToolBar {
        id: toolBar
        y: 0
        x: 0

        background: Rectangle {
            color:"transparent"
            anchors.fill: parent
        }

        Row {
            id: toolsRow
            height: 32
            Repeater {
                id: repeater
                model: dropdown.toolsTitles ? dropdown.toolsTitles : 0
                delegate: ToolButton {

                    property bool isTurnOn : modelData[2]
                    id: button
                    flat: true
                    height: parent.height
                    enabled: modelData != null
                    contentItem: Label {
                            text: modelData ? modelData[0] : ""
                            color: "#ffffff"
                        }
                    onClicked: {
                        dropdown.itemDidClick(modelData[1], button);
                    }

                    background: Rectangle {
                                anchors.fill: parent
                                color: button.isTurnOn || button.down ? "#5d5d5d" : "transparent"
                            }
                }

                onCountChanged: {
                    dropdown.updateFrame();
                }

            }
        }
    }
}
