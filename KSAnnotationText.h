#ifndef KSANNOTATIONTEXT_H
#define KSANNOTATIONTEXT_H

#include <QQuickItem>
#include <QtQuick>
#include "ksAnnotationItem.h"


#define DEBUG_TEST_REMOT_ENABLED 0

class KSAnnotationText : public KSAnnotationItem
{
    Q_OBJECT

    Q_PROPERTY(bool isBold READ isBold WRITE setIsBold NOTIFY isBoldChanged)
    Q_PROPERTY(bool isItalic READ isItalic WRITE setIsItalic NOTIFY isItalicChanged)
    Q_PROPERTY(double fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)

public:

    KSAnnotationText(QQuickItem *parent = 0);
    ~KSAnnotationText();

    bool isBold() const
    {
        return m_isBold;
    }

    bool isItalic() const
    {
        return m_isItalic;
    }

    double fontSize() const
    {
        return m_fontSize;
    }

public slots:

    void setIsBold(bool isBold)
    {
        if (m_isBold == isBold)
            return;

        m_isBold = isBold;
        emit isBoldChanged(m_isBold);
    }

    void setIsItalic(bool isItalic)
    {
        if (m_isItalic == isItalic)
            return;

        m_isItalic = isItalic;
        emit isItalicChanged(m_isItalic);
    }

    void setFontSize(double fontSize)
    {
        qWarning("Floating point comparison needs context sanity check");
        if (qFuzzyCompare(m_fontSize, fontSize))
            return;

        m_fontSize = fontSize;
        emit fontSizeChanged(m_fontSize);
    }

signals:


    void isBoldChanged(bool isBold);

    void isItalicChanged(bool isItalic);

    void fontSizeChanged(double fontSize);

private:

    bool m_isBold;
    bool m_isItalic;
    double m_fontSize;
};

#endif // KSANNOTATIONTEXT_H
