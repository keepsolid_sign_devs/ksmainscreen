import QtQuick 2.0

Item {
    id: item1
    Text {
        id: text1
        x: 265
        y: 233
        width: 143
        height: 17
        text: qsTr("Documents View !!!")
        font.family: "Verdana"
        styleColor: "#00000000"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenterOffset: 0
        anchors.verticalCenter: parent.verticalCenter
        transformOrigin: Item.Center
        font.pixelSize: 14
    }

}
