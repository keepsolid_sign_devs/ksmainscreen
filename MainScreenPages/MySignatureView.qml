import QtQuick 2.0
import QtQuick.Controls 2.0
import com.keepsolid.app.paintview 1.0
import "../ui/" as KSControls
import Theme 1.0
import QtQuick.Dialogs 1.0

Rectangle {
    id: mySignatureView



    property variant paintersList: [qsTr("My Signature"),qsTr("My Initial")]
    property variant sourceList: ["qrc:/resources/test1.png", "qrc:/resources/test2.png"]

    onWidthChanged: {
        console.log("onWidthChanged: ",width);
    }
    onHeightChanged: {
        console.log("onHeightChanged: ",height);
    }

    Rectangle {
        id: centreView
        width: {
            var newWidth = 100;
            if(paintRepeate.itemAt(previewList.currentIndex) != null)
            {
                if(paintRepeate.itemAt(previewList.currentIndex).paintView.implicitWidth > 0 )
                {
                    newWidth = paintRepeate.itemAt(previewList.currentIndex).paintView.implicitWidth
                            + paintRepeate.itemAt(previewList.currentIndex).anchors.leftMargin
                            + paintRepeate.itemAt(previewList.currentIndex).anchors.rightMargin
                            + previewList.width
                            + previewList.anchors.leftMargin;// - 55 - 95;
                }
            }

            return newWidth;
        }

        height: {
            var newHeight = 100;
            if(paintRepeate.itemAt(previewList.currentIndex) != null)
            {
                if(paintRepeate.itemAt(previewList.currentIndex).paintView.implicitHeight > 0 )
                {
                    newHeight = paintRepeate.itemAt(previewList.currentIndex).paintView.implicitHeight;// - 132;
                }
            }

            return newHeight;
        }
        anchors.top: parent.top
        anchors.topMargin: 32
        anchors.horizontalCenter: parent.horizontalCenter

        ListView {
            id: previewList

            spacing: 15
            x: 28
            objectName: "leftMenuList"
            width: 130
            anchors.left: centreView.left
            anchors.leftMargin: 17
            anchors.bottom: centreView.bottom
            anchors.bottomMargin: 18
            anchors.top: centreView.top
            anchors.topMargin: 10
            model: paintersList

            delegate: KSControls.CSTitledImagePreview
            {
                source: paintRepeate.itemAt(index) ? paintRepeate.itemAt(index).paintView.source : null
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                height: 145
                isSelected: index == previewList.currentIndex
                borderWidth: 2
                titleLabel: model[index]

                onClicked: {
                    previewList.currentIndex = index
                }
            }
        }

        Repeater {
            id: paintRepeate
            model: mySignatureView.paintersList

            delegate:
                Rectangle {
                color: "transparent"
                property var paintView : painter
                anchors.right: centreView.right
                anchors.rightMargin: 19
                anchors.bottom: centreView.bottom
                anchors.bottomMargin: 137
                anchors.left: previewList.right
                anchors.leftMargin: 25
                anchors.top: centreView.top
                anchors.topMargin: 10

                KSControls.CSPaintView {
                    id: painter

                    width: implicitWidth
                    height: implicitHeight
                    painter: mySignatureView.paintersList[index]
                    visible: previewList.currentIndex == index
                    scale: (painter == null
                            || item.textureWidth == 0
                            || item.textureHeight == 0
                            ? 1
                            : Math.min((mySignatureView.width
                                        -  previewList.width
                                        - previewList.anchors.leftMargin - 65)
                                       /painter.textureWidth
                                       ,(mySignatureView.height
                                         - 20
                                         - paintRepeate.itemAt(previewList.currentIndex).anchors.bottomMargin)
                                       /painter.textureHeight))

                    onScaleChanged: {
                        console.log("painter: ", painter, "scale: ", scale, " mySignatureView.width: ",mySignatureView.width," painter.textureWidth: ",painter.textureWidth);
                    }

                    Component.onCompleted: {
                        //                    painter.source = mySignatureView.sourceList[index]
                    }

                    onWidthChanged: {
                        console.log("CSPaintView onWidthChanged: ", painter.width);
                    }
                    onHeightChanged: {
                        console.log("CSPaintView onHeightChanged: ", painter.height);
                    }
                }
            }


        }

        Button {
            id: saveBtn
            text: qsTr("Save")
            anchors.top: centreView.bottom
            anchors.topMargin: 20
            anchors.right: centreView.right
            anchors.rightMargin: 30

            onClicked: {
                paintRepeate.itemAt(previewList.currentIndex).paintView.save();
                //            mySignatureView.sourceList[previewList.currentIndex] = paintRepeate.itemAt(previewList.currentIndex).paintView.source;

                console.log(paintRepeate.itemAt(previewList.currentIndex).paintView);
                console.log(paintRepeate.itemAt(previewList.currentIndex).paintView.source);
            }

        }

        Button {
            id: importBtn
            text: qsTr("Import")
            anchors.top: saveBtn.top
            anchors.topMargin: 0
            anchors.right: saveBtn.left
            anchors.rightMargin: 12
            onClicked: {
                console.log("Import");
                fileDialog.visible = true;
            }
        }
    }


    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        onAccepted: {
            paintRepeate.itemAt(previewList.currentIndex).paintView.importFile(fileDialog.fileUrls);
            console.log("You chose: " + fileDialog.fileUrls)

        }
        onRejected: {
            console.log("Canceled")

        }
    }
}
