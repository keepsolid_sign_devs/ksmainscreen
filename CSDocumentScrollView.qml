import QtQuick 2.0
import Theme 1.0
import com.keepsolid.app.paintview 1.0
import QtQuick.Controls 2.0
import "./DocumentEditor/DocumentPage" as KSDocumentPage

Rectangle {

    id: documentView
    objectName: "documentView"

    function page(pageIndex){
        var loader = docRepeater.itemAt(pageIndex).loader;
        return loader.item;
    }

    function isActive(pageIndex)
    {
        var loader = docRepeater.itemAt(pageIndex).loader;
        return loader.active;
    }

    function activatePage(pageIndex) {
        var loader = docRepeater.itemAt(pageIndex) ? docRepeater.itemAt(pageIndex).loader : null;
        if(loader != null)
        {
            loader.active = true;
        }
        else
        {
            console.log("page loader activation error: ",pageIndex);
        }
    }


    Flickable
    {
        id:documentScrollGround
        objectName: "Flickable"
        anchors.fill: parent

        contentWidth:columnPages.width    //Flickable
        contentHeight:columnPages.height
        ScrollBar.vertical: ScrollBar { }
        ScrollBar.horizontal: ScrollBar { }

        MouseArea {
            id:groundClick
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: {
                console.log("ground clicked");

                if(contextMenu != null)
                    contextMenu.hide(true);
                documentController.currentActiveAnnotation = null;
                if(mouse.button == Qt.LeftButton)
                {
                    documentController.zoom(documentController.documentScale + 0.1);
                }
                else
                {
                    documentController.zoom(documentController.documentScale - 0.1);
                }


            }
        }

        Column {
            id:columnPages
            objectName: "pageColumn"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 0

            Repeater {
                id: docRepeater
                objectName: "docRepeater"
                parent: columnPages
                model: documentController.pdf.pageCount

                delegate:  Item {

                    property alias loader: pageLoader
                    id: pageTmpItem
                    width: documentScrollGround.width
                    height: documentController.pageSize(index).height//*documentController.documentScale\

                    Binding {
                        target: pageLoader
                        property: "active"
                        value: true
                        when: (documentScrollGround.height > 0
                               && documentScrollGround.contentHeight > 0
                               && (pageTmpItem.y >= documentScrollGround.contentY
                                   && pageTmpItem.y < documentScrollGround.contentY + documentScrollGround.height))
                    }

                    Loader {
                        id: pageLoader

                        active: false
                        anchors.fill: parent


                        sourceComponent: KSDocumentPage.DocumentPage {
                            id: page
                            objectName: "page %1".arg(index)
                            pageIndex: index
                            isActive: true
                            pageWidth: documentController.pageSize(index).width//*documentController.documentScale
                            pageHeight: documentController.pageSize(index).height//*documentController.documentScale
//                            scale: documentController.documentScale

                            anchors.horizontalCenter: parent.horizontalCenter
                            width: documentScrollGround.width
                            currentActiveAnnotation: documentController.currentActiveAnnotation

                            onAnnotationDidPress: {
                                if(contextMenu != null)
                                    contextMenu.hide(true);//***
                                contextMenu = dropdown
                                documentController.currentActiveAnnotation = sender;
                            }

                            onAnnotationDidRelease: {
                                contextMenu = dropdown
                                contextMenu.updateFrame();
                                contextMenu.showDelayed();
                            }

                            onPageReady: {
                                documentController.pageReady(index);
                            }

                            Component.onCompleted: {
                                documentController.setPageActivationIndex(index);
                            }
                        }

                    }

                }







            }
        }

        onContentYChanged: {
            if(contextMenu != null)
                contextMenu.updateFrameDelayed();
        }
    }

    Connections {
        target: paintWindow
        onVisibleChanged: {
            if(paintWindow.accepted)
            {
                documentController.currentActiveAnnotation.kernel.kernelData = paintWindow.source;
            }
        }
    }

    Connections {
        target: textEditWindow
        onVisibleChanged: {
            if(textEditWindow.accepted)
            {
                documentController.currentActiveAnnotation.kernel.kernelData = textEditWindow.text;
            }
        }
    }
}
