#include "KSPaintView.h"
#include "theme.h"
#include <QFileInfo>
#include <QPainter>
#include <QGraphicsEffect>
#include <QGraphicsPixmapItem>
#include <QGraphicsColorizeEffect>
#include <QGraphicsScene>
#include "ksimageprovider.h"
#include "ksgraphiceffectshelper.h"

KSPaintView::KSPaintView(QQuickItem *parent)
    : QQuickPaintedItem(parent),
      polyWidth(SIGNATURE_WIDTH),
      polyHeight(SIGNATURE_HEIGHT),
      m_signatureImage(SIGNATURE_WIDTH,SIGNATURE_HEIGHT,QImage::Format_RGBA8888),
      m_color(QColor("#5c2b85")),
      m_scale(1),
      m_brushStyle(PaintViewPen),
      m_source(QUrl::fromLocalFile("C:\\Users\\user\\Documents\\test1.png"))
{
    connect(this,SIGNAL(sourceChanged(QUrl)),SLOT(loadImage(QUrl)));


    //    QImage img(":\\resources\\zebra--steel.png");
    //    makeFromImage(img);
}

void KSPaintView::drawSignature(QPainter *painter)
{
    qreal dx = xOffset();
    qreal dy = yOffset();
    painter->translate(dx,dy);

    painter->drawImage(0,0, m_signatureImage);
    drawPath(painter);
}

void KSPaintView::drawPath(QPainter *painter)
{
    QBrush brush(color());

    QPen pen(brush,4,Qt::SolidLine, Qt::RoundCap,Qt::BevelJoin);
    pen.setCapStyle(Qt::SquareCap);

    painter->setBrush(brush);
    painter->setPen(pen);
    painter->setRenderHint(QPainter::HighQualityAntialiasing);

    if(poly.count() > 1)
    {
        if(brushStyle() == PaintViewEarase)
        {
            pen.setWidth(18);
            painter->setCompositionMode(QPainter::CompositionMode_Clear);
            painter->strokePath(poly.GetVectorPath(),pen);
        }
        else if(brushStyle() == PaintViewNarrow)
        {

            QColor fC = pen.color();
            QColor pC = fC;
            pC.setAlpha(pC.alpha()*0.5);

            QPainterPath path = poly.GetPath();


            qreal xOffset = 1;
            int nOffset = 20;
            path.translate(xOffset*(-nOffset*0.5),0);

            for(int i = 0; i < nOffset; i++)
            {
                if(i == 0 || i == nOffset - 1)
                {
                    pen.setWidth(2);
                    pen.setColor(pC);
                }
                else
                {
                    pen.setWidth(1);
                    pen.setColor(fC);
                }

                path.translate(xOffset,0);
                painter->strokePath(path,pen);
            }

//            QPainterPath path = poly.GetNarrowPath();

//            //            QList<QPolygonF> p2 = narrowPath.toSubpathPolygons();

//            //            if(p2.count() > 0)
//            //            {
//            //                foreach (QPolygonF p, p2) {
//            //                    painter->drawPolygon(p);
//            //                }
//            //            }

//            //            m_narrowPath = QPainterPath();
//            //            m_narrowPath.addPolygon(p1[0]);
//            //            m_narrowPath.addPolygon(p2[0]);

//            //            path.setFillRule(Qt::WindingFill);
//            //            path = path.simplified();

//            painter->setRenderHint(QPainter::Antialiasing);
//            path.setFillRule(Qt::WindingFill);
//            painter->fillPath(path,brush);


//            painter->setRenderHint(QPainter::HighQualityAntialiasing);
//            pen.setWidth(1);
//            QColor c = pen.color();
//            c.setAlpha(c.alpha()*0.5);
//            pen.setColor(c);
//            painter->strokePath(path,pen);



//            //            pen.setColor("#ff0000");
//            //            pen.setWidth(1);

//            //            int radiusX = 5;
//            //            int radiusY = 0;
//            //            poly.translate(+radiusX,radiusY);
//            //            painter->strokePath(poly.GetVectorPath(),pen);


//            //            poly.translate(-radiusX,-radiusY);
//            //            poly.translate(-radiusX,-radiusY);

//            //            painter->strokePath(poly.GetVectorPath(),pen);
//            //            poly.translate(+radiusX,radiusY);




//            //            pen.setWidth(5);
//            //            RoundedPolygon poly2 = poly;
//            //            poly2.translate(2,0);
//            //            painter->strokePath(poly2.GetPath(),pen);
//            //            RoundedPolygon poly3 = poly;
//            //            poly3.translate(-2,0);
//            //            painter->strokePath(poly3.GetPath(),pen);
        }
        else if(brushStyle() == PaintViewPen)
        {
            pen.setWidth(6);
            painter->strokePath(poly.GetPath(),pen);
            //            QBrush brush2("#ff0000");

            //            painter->fillPath(poly.GetPath2(),brush2);
        }
    }
}

void KSPaintView::paint(QPainter *painter)
{
    painter->save();

    qreal dx = xOffset();
    qreal dy = yOffset();

    QRect outputRect(dx*m_scale,dy*m_scale,m_signatureImage.width()*m_scale,m_signatureImage.height()*m_scale);

    QBrush brush(Theme::instance()->whiteTwoColor());
    QPen pen(brush,2,Qt::DashLine);

    pen.setDashPattern( QVector<qreal>() << 10 << 5);
    painter->setBrush(QBrush("transparent"));
    painter->setPen(pen);
    painter->setRenderHint(QPainter::HighQualityAntialiasing);

    if(outputRect.top() > 0)
    {
        painter->drawLine(QPoint(outputRect.left(),outputRect.top()),QPoint(outputRect.right(),outputRect.top()));
        painter->drawLine(QPoint(outputRect.left(),outputRect.bottom()),QPoint(outputRect.right(),outputRect.bottom()));
    }

    if(outputRect.left() > 0)
    {
        painter->drawLine(QPoint(outputRect.left(),outputRect.top()),QPoint(outputRect.left(),outputRect.bottom()));
        painter->drawLine(QPoint(outputRect.right(),outputRect.top()),QPoint(outputRect.right(),outputRect.bottom()));
    }

    painter->setClipRect(outputRect);

    painter->scale(m_scale,m_scale);

    drawSignature(painter);

    painter->restore();
}

qreal  KSPaintView::xOffset()
{
    return (width() - m_signatureImage.width()*m_scale)*0.5/m_scale;//trunc
}

qreal  KSPaintView::yOffset()
{
    return (height() - m_signatureImage.height()*m_scale)*0.5/m_scale;
}

void KSPaintView::addUIPoint(QPoint point)
{
    qreal dx = xOffset();
    qreal dy = yOffset();
    point.setX(point.x()/m_scale - dx);
    point.setY(point.y()/m_scale - dy);

    if(poly.count() >= 1 && brushStyle() != PaintViewEarase)
    {
        QPoint p(point.x() - poly.at(poly.count() - 1).x(), point.y() - poly.at(poly.count() - 1).y());
        qreal r = qSqrt(p.x()*p.x() + p.y()*p.y());

        if(r <= 15)
        {
            qDebug("ignore");
            return;
        }
    }


    //    if(poly.count() >= 2)
    //    {
    //        QPoint p1 = poly[poly.count() - 2];
    //        QPoint p2 = point;
    //        QPoint p3 = poly[poly.count() - 1];

    //        QVector2D v1(p2.x() - p1.x(), p2.y() - p1.y());
    //        v1.normalize();
    //        QVector2D v2(p3.x() - p1.x(), p3.y() - p1.y());
    //        v2.normalize();
    //        QVector2D v3(p3.x() - p2.x(),p3.y() - p2.y());
    //        v3.normalize();

    //        qreal dot1 = QVector2D::dotProduct(v2,v1);
    //        qreal dot2 = QVector2D::dotProduct(v3,v1);
    //        if(qAbs(dot1 - 1.0) < 0.000000000001 && dot2 < 0)
    //        {
    //            poly.pop_back();
    //        }

    //    }

    poly.push_back(point);
}

void KSPaintView::startPoint(QPoint point)
{
    addUIPoint(point);
}

void KSPaintView::endPoint(QPoint point)
{
    addUIPoint(point);
    applyImage();
    poly.clear();
    update();
}

void KSPaintView::nextPoint(QPoint point)
{

    if(brushStyle() == PaintViewNarrow)
    {

//        if(poly.count() > 2)
//        {
//            QPoint a = poly[poly.count() - 1];
//            QPoint b = poly[poly.count() - 2];
//            QPoint c = poly[poly.count() - 2];

//            QPoint ab(a.x() - b.x(),a.y() - b.y());
//            QPoint bc(b.x() - c.x(),b.y() - c.y());

//            bool isClosePath = ((ab.x() > 0 && bc.x() < 0)
//                                || (ab.x() < 0 && bc.x() > 0)
//                                || (ab.y() > 0 && bc.y() < 0)
//                                || (ab.y() < 0 && bc.y() > 0));


//            poly.setClosePath(isClosePath);

//            applyImage();
//            poly.setClosePath(false);

//            //            QPoint l1 = poly[poly.count() - 1];
//            //            QPoint l2 = poly[poly.count() - 2];
//            //            poly.clear();
//            //            addUIPoint(l2);
//            //            addUIPoint(l1);

//            for(int i = 0; i < 1; i++)
//            {
//                poly.removeFirst();
//            }
//        }


    }

    addUIPoint(point);

    update();
}

void KSPaintView::setColor(QColor color)
{
    if (m_color == color)
        return;

    m_color = color;

    QPainter painter(&m_signatureImage);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.fillRect(m_signatureImage.rect(), m_color);
    painter.end();

    update();

    emit colorChanged(m_color);
}

void KSPaintView::clear()
{
    m_signatureImage = QImage(SIGNATURE_WIDTH,SIGNATURE_HEIGHT,QImage::Format_RGBA8888);
    m_signatureImage.fill(Qt::white);
    m_signatureImage.fill("transparent");
    poly.clear();
    update();
}

void KSPaintView::applyImage()
{
    QPainter pathPainter(&m_signatureImage);
    drawPath(&pathPainter);
    pathPainter.end();

}

int  KSPaintView::implicitHeight() const
{
    return textureHeight()*m_scale;
}

int  KSPaintView::implicitWidth() const
{
    return textureWidth()*m_scale;
}

int  KSPaintView::textureWidth() const
{
    qDebug () << "m_signatureImage.width() " << m_signatureImage.width();
    return m_signatureImage.width();
}
int  KSPaintView::textureHeight() const
{
    return m_signatureImage.height();
}

void KSPaintView::save()
{
    applyImage();

    QString imgPath = m_source.toLocalFile();
    if(imgPath.isEmpty())
    {
        imgPath = ":" + m_source.path();

        QFileInfo info(imgPath);
        imgPath = info.fileName();
        imgPath = "C:\\Users\\user\\Documents\\" + imgPath;
    }

    if(QFile::remove(imgPath))
    {
        qDebug() << "remove file: "<< imgPath;
    }

    if(imgPath.endsWith("_m.png"))
    {
        imgPath = imgPath.left(imgPath.length() - 6) + ".png";
    }
    else if(imgPath.endsWith(".png"))
    {
        imgPath = imgPath.left(imgPath.length() - 4) + "_m.png";
    }

    if( m_signatureImage.save(imgPath,"PNG")) {
        QUrl url = QUrl::fromLocalFile(imgPath);
        m_source = url;
        emit sourceChanged(m_source);
        qDebug() << "file saved: " << imgPath;
    }
    else {
        qDebug() << "Can't open file: " << imgPath;
    }
}

void KSPaintView::importFile(QString url)
{
    QUrl gurl(url);
    url = gurl.toLocalFile();

    QImage img(url);
    makeFromImage(img);

}

void KSPaintView::loadImage(QUrl source)
{
    poly.clear();

    QString path = source.path();

    QString providerPrefix = "image://myprovider/";
    if(path.startsWith(providerPrefix))
    {
        path.remove(0,providerPrefix.length());
        QSize size(SIGNATURE_WIDTH,SIGNATURE_HEIGHT);
        QImage img = KSImageProvider::instance()->requestImage(path,&size, size);
        setSignatureImage(img);
    }
    else
    {
        QString imgPath = source.toLocalFile();
        if(imgPath.isEmpty())
        {
            imgPath = ":" + path;
        }
        QImage img(imgPath);
        setSignatureImage(img);
    }

    update();
}

void KSPaintView::setScale(qreal scale)
{
    if (qFuzzyCompare(m_scale, scale) || scale < 0)
        return;

    m_scale = scale;

    emit implicitWidthChanged(implicitWidth());
    emit implicitHeightChanged(implicitHeight());

    emit scaleChanged(m_scale);

    qDebug() << "current scale: " << m_scale;

    update();
}

void KSPaintView::makeFromImage(QImage source)
{

    source = cleanUpAlpha(source);
    source = makeGray(source);
    source = medianProcess(source, 1);


    source = changeBrightness(source, 20);
    source = applyLevels(source,1.0,1.0,1.0,1.0);

    source = changeContrast(source, 100);


//

    int thrhld = getMedianGrey(source,0);
    source = getThresholdBW(source, thrhld,true);

    source = blurredImage(source);

    m_signatureImage = source.scaled(QSize(SIGNATURE_WIDTH,SIGNATURE_HEIGHT));
    update();
}


QImage KSPaintView::applyEffectToImage(QImage src, QGraphicsEffect *effect)
{
    if(src.isNull()) return QImage(); //No need to do anything else!
    if(!effect) return src; //No need to do anything else!

    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    item.setPixmap(QPixmap::fromImage(src));
    item.setGraphicsEffect(effect);
    scene.addItem(&item);
    QImage res(QSize(SIGNATURE_WIDTH,SIGNATURE_HEIGHT), QImage::Format_ARGB32);
    res.fill(Qt::transparent);
    QPainter ptr(&res);
    scene.render(&ptr, QRectF(), QRectF( 0, 0, SIGNATURE_WIDTH, SIGNATURE_HEIGHT ) );
    return res;
}

