#ifndef KSASYNCPAGEPROVIDER_H
#define KSASYNCPAGEPROVIDER_H

#include <QQuickItem>
#include <qquickimageprovider.h>
#include <QDebug>
#include <QImage>
#include <QThreadPool>

#include "KSAsyncPageResponse.h"

class KSAsyncPageProvider : public QQuickAsyncImageProvider
{
public:
    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize)
    {
        KSAsyncPageResponse *response = new KSAsyncPageResponse(id, requestedSize);
        pool.start(response);
        return response;
    }

    static KSAsyncPageProvider *instance()
    {
        static KSAsyncPageProvider *provider = new KSAsyncPageProvider();
        return provider;
    }

private:
    QThreadPool pool;
};

#endif // KSASYNCPAGEPROVIDER_H
