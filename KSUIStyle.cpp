#include "KSUIStyle.h"

KSUIStyle::KSUIStyle(QObject *parent) :
    QObject(parent),
    m_elevation(0)
{

}

int KSUIStyle::elevation() const
{
    return m_elevation;
}

void KSUIStyle::setElevation(int elevation)
{
    if (elevation == m_elevation)
        return;

    m_elevation = elevation;
    emit elevationChanged();
}
