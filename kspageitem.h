#ifndef KSPAGEITEM_H
#define KSPAGEITEM_H

#include <QtQuick>
#include "ksAnnotationItem.h"
#include "KSAnnotationListModel.h"

class PageRenderer;

class KSPageItem : public QQuickItem
{
    Q_OBJECT
public:
    Q_PROPERTY(KSAnnotationListModel* annotationsModel READ annotationsModel CONSTANT)
    Q_PROPERTY(int pageIndex READ pageIndex WRITE setPageIndex NOTIFY pageIndexChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setIsActive NOTIFY isActiveChanged)

    KSPageItem(QQuickItem *parent = 0);
    ~KSPageItem();

    KSAnnotationListModel* annotationsModel() const
    {
        return m_annotationsModel;
    }

    int pageIndex() const
    {
        return m_pageIndex;
    }

    bool isActive() const
    {
        return m_isActive;
    }

public slots:

    void setPageIndex(int pageIndex)
    {
        if (m_pageIndex == pageIndex)
            return;

        m_pageIndex = pageIndex;
        emit pageIndexChanged(m_pageIndex);
    }

    void setIsActive(bool isActive)
    {
        if (m_isActive == isActive)
            return;

        m_isActive = isActive;
        if(m_isActive)
        {
            update();
        }
        else
        {
            qDebug("*WARNING!!! page deactivation. This should never happen.");
        }
        emit isActiveChanged(m_isActive);
    }

signals:

    void pageIndexChanged(int pageIndex);

    void isActiveChanged(bool isActive);

private:

    KSAnnotationListModel *m_annotationsModel;
    int m_pageIndex;
    bool m_isActive;
};

#endif // KSPAGEITEM_H
