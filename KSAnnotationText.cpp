#include "KSAnnotationText.h"
#include "theme.h"


KSAnnotationText::KSAnnotationText(QQuickItem *parent)
    : KSAnnotationItem(parent)
    , m_isBold(false)
    , m_isItalic(false)
    , m_fontSize(12)
{

};

KSAnnotationText::~KSAnnotationText()
{

}
