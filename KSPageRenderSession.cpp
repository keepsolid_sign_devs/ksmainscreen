#include "KSPageRenderSession.h"

KSPageRenderSession::KSPageRenderSession(QObject *parent) : QObject(parent), m_pdf(NULL)
{

}

QThread* KSPageRenderSession::addThread(int page, qreal zoom)
{
    if(m_pdf == NULL)
    {
        return NULL;
    }

    if (_zoomCache.contains(page)
            && _zoomCache[page] > zoom)
    {
        emit imageReady(page, *_imageCache[page]);
        return NULL;
    }

    KSPageRenderWorker* worker = new KSPageRenderWorker(this,page,zoom);
    worker->setDocument(m_pdf);

    QThread* thread = new QThread;
    worker->moveToThread(thread);

    connect(thread, SIGNAL(started()), worker, SLOT(process()));

    connect(worker,SIGNAL(pageReady(int,qreal, const QImage)),SLOT(pageLoaded(int,qreal, const QImage)));

    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));

    connect(this, SIGNAL(stopAll()), worker, SLOT(stop()));

    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));

    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    thread->start();

    return thread;
}

void KSPageRenderSession::stopThreads()
{
    emit  stopAll();
}

KSPageRenderSession::~KSPageRenderSession()
{
    stopThreads();  /* останавливаем и удаляем потоки  при окончании работы сессии */

}

void  KSPageRenderSession::pageLoaded(int page, qreal zoom, const QImage image)
{
    if (_zoomCache.contains(page)
            && _zoomCache[page] > zoom)
    {
        return;
    }
    _imageCache.insert(page, new QImage(image));
    _zoomCache.insert(page, zoom);

    emit imageReady(page, *_imageCache[page]);
}
