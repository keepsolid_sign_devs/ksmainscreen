#include "AppController.h"
#include <QDebug>

AppController::AppController() :
    m_elevation(0),
    m_docController(NULL)
{

}

void AppController::lock()
{

}

int AppController::elevation() const
{
    return m_elevation;
}

void AppController::setElevation(int elevation)
{
    if (elevation == m_elevation)
        return;

    m_elevation = elevation;
    emit elevationChanged();
}
