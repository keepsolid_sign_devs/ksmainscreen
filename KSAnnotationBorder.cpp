#include "KSAnnotationBorder.h"
#include "theme.h"

KSAnnotationBorder::KSAnnotationBorder(QQuickItem *parent)
    : QQuickPaintedItem(parent)
    , m_isActive(false)
{
    setAcceptHoverEvents(true);

}

void KSAnnotationBorder::hoverEnterEvent(QHoverEvent *event)
{
    QQuickPaintedItem::hoverEnterEvent(event);
    setIsHovered(true);
}

void KSAnnotationBorder::hoverLeaveEvent(QHoverEvent *event)
{
    QQuickPaintedItem::hoverLeaveEvent(event);
    setIsHovered(false);
}

void KSAnnotationBorder::paint(QPainter *painter)
{
    painter->save();

    QRect rect(0,
               0,
               trunc(width() - 0.5),
               trunc(height() - 0.5));

    painter->setRenderHint(QPainter::Antialiasing);

    QColor groundColor = "transparent";//m_color;
//    groundColor.setAlpha(90);//13

    QColor borderColor = m_color;
//    borderColor.setAlpha(26);

    QBrush groundBrush(groundColor);
    QBrush borderBrush(borderColor);

    QPen pen(borderBrush,borderWidth());
//    if(isActive() == false)
//    {
//        pen.setDashPattern( QVector<qreal>() << 4 << 2);
//    }

    painter->setPen(pen);
    painter->setBrush(groundBrush);

    painter->drawRect(rect);

    painter->restore();
}
