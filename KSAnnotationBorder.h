#ifndef KSANNOTATIONBORDER_H
#define KSANNOTATIONBORDER_H

#include <QtQuick>

class KSAnnotationBorder : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY isColor)
    Q_PROPERTY(bool isActive READ isActive WRITE setIsActive NOTIFY isActiveChanged)
    Q_PROPERTY(int borderWidth READ borderWidth CONSTANT)
    Q_PROPERTY(bool isHovered READ isHovered WRITE setIsHovered NOTIFY isHoveredChanged)

public:
    KSAnnotationBorder(QQuickItem *parent = 0);
    void hoverEnterEvent(QHoverEvent *event);
    void hoverLeaveEvent(QHoverEvent *event);

    void paint(QPainter *painer);
    bool isActive() const
    {
        return m_isActive;
    }
    QColor color() const
    {
        return m_color;
    }

    int borderWidth() const
    {
        return isActive() || isHovered() ? 3 : 1;
    }

    bool isHovered() const
    {
        return m_isHovered;
    }

public slots:
    void setIsActive(bool isActive)
    {
        if (m_isActive == isActive)
            return;

        m_isActive = isActive;
        emit isActiveChanged(m_isActive);
        update();
    }
    void setColor(QColor color)
    {
        if (m_color == color)
            return;

        m_color = color;
        emit isColor(m_color);
        update();
    }

    void setIsHovered(bool isHovered)
    {
        if (m_isHovered == isHovered)
            return;

        m_isHovered = isHovered;
        emit isHoveredChanged(m_isHovered);
        update();
    }

signals:
    void isActiveChanged(bool isActive);

    void isColor(QColor color);

    void isHoveredChanged(bool isHovered);

private:
    bool m_isActive;
    QColor m_color;
    bool m_isHovered;
};

#endif // KSANNOTATIONBORDER_H
