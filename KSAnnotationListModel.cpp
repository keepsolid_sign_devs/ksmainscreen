#include "KSAnnotationListModel.h"


KSAnnotationListModel::KSAnnotationListModel(QObject *parent)
    : QAbstractListModel(parent) {

    insertRows(0, KSAnnotationItem::AnnotationTypes::AnnotationTypesMax, QModelIndex());

    for(int i = 0; i < KSAnnotationItem::AnnotationTypes::AnnotationTypesMax; i++)
    {
//        if(i != KSAnnotationItem::AnnotationTypes::AnnotationDate)
        {
            QModelIndex index = KSAnnotationListModel::index(i, 0, QModelIndex());
            setData(index, i);
        }
    }
}


/*!
    Returns the number of items in the string list as the number of rows
    in the model.
*/

QHash<int, QByteArray> KSAnnotationListModel::roleNames() const
{
   QHash<int, QByteArray> roles;
   roles[amFile] = "qmlFile";
   roles[amType] = "annotationType";
   roles[amParticipant] = "annotationParticipant";
   return roles;
}


int KSAnnotationListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_annotations.count();
}

QModelIndex KSAnnotationListModel::index(int row, int column,
                 const QModelIndex &parent) const {

   Q_UNUSED(parent);
   return createIndex(row, column, static_cast<void *>(0));
}

/*!
    Returns an appropriate value for the requested data.
    If the view requests an invalid index, an invalid variant is returned.
    Any valid index that corresponds to a string in the list causes that
    string to be returned.
*/

QVariant KSAnnotationListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_annotations.size())
        return QVariant();

    if (role == amFile)
        return (m_annotations.at(index.row()) == KSAnnotationItem::AnnotationTypes::AnnotationText
                || m_annotations.at(index.row()) == KSAnnotationItem::AnnotationTypes::AnnotationDate
                ? QVariant("KSTextAnnotation.qml")
                : QVariant("KSImageAnnotation.qml"));
    else if (role == amType)
    {
        return m_annotations.at(index.row());
    }
    else if (role == amParticipant)
    {
        return QVariant("Participant 1");
    }
    else
        return QVariant();
}

/*!
    Returns the appropriate header string depending on the orientation of
    the header and the section. If anything other than the display role is
    requested, we return an invalid variant.
*/


QVariant KSAnnotationListModel::headerData(int section, Qt::Orientation orientation,
                                     int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
        return QString("Column %1").arg(section);
    else
        return QString("Row %1").arg(section);
}


/*!
    Returns an appropriate value for the item's flags. Valid items are
    enabled, selectable, and editable.
*/


Qt::ItemFlags KSAnnotationListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}


/*!
    Changes an item in the string list, but only if the following conditions
    are met:

    * The index supplied is valid.
    * The index corresponds to an item to be shown in a view.
    * The role associated with editing text is specified.

    The dataChanged() signal is emitted if the item is changed.
*/


bool KSAnnotationListModel::setData(const QModelIndex &index,
                              const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {

        m_annotations.replace(index.row(), value.toInt());
        emit dataChanged(index, index);
        return true;
    }

    return false;
}


/*!
    Inserts a number of rows into the model at the specified position.
*/


bool KSAnnotationListModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        m_annotations.insert(position, KSAnnotationItem::AnnotationTypes::AnnotationText);
    }

    endInsertRows();
    return true;

}


/*!
    Removes a number of rows from the model at the specified position.
*/


bool KSAnnotationListModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        m_annotations.removeAt(position);
    }

    endRemoveRows();
    return true;

}


QVariantMap KSAnnotationListModel::get(int row) {
    QHash<int,QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    while (i.hasNext()) {
        i.next();
        QModelIndex idx = index(row, 0);
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
        //cout << i.key() << ": " << i.value() << endl;
    }
    return res;
}

