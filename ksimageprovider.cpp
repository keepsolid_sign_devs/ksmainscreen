#include "ksimageprovider.h"

//KSImageProvider::KSImageProvider()
//{

//}

KSImageProvider::~KSImageProvider()
{
    delete manager;
}

QImage KSImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(requestedSize);
    try
        {
        QUrl url("https://unsplash.it/" + id);
        QNetworkReply* reply = manager->get(QNetworkRequest(url));
        QEventLoop eventLoop;
        QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
        eventLoop.exec();
        if (reply->error() != QNetworkReply::NoError)
        {
            reply->deleteLater();
            return QImage();
        }
        QImage image = QImage::fromData(reply->readAll());

        reply->deleteLater();
        reply = nullptr;

        if(image.isNull())
        {
            return  QImage();
        }
        size->setWidth(image.width());
        size->setHeight(image.height());
        return image;

        }
        catch (int)
        {
            return  QImage();
        }

    return  QImage();
}

//QImage KSImageProvider::requestImage(const QString & id, QSize * size, const QSize & requestedSize)
//{
//    Q_UNUSED(id);

//    int width = 160;
//    int height = 100;

//    if(size)
//        *size = QSize(width,height);
//    QPixmap pixmap(requestedSize.width() > 0 ? requestedSize.width() : width,
//                   requestedSize.height() > 0 ? requestedSize.height() : height);
//    pixmap.fill(QColor("blue").rgba());
//    return pixmap.toImage();
//}
