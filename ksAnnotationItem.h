#ifndef KSANNOTATIONITEM_H
#define KSANNOTATIONITEM_H

#include <QQuickItem>
#include <QtQuick>


#define DEBUG_TEST_REMOT_ENABLED 0

class KSAnnotationItem : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QString kernelData READ kernelData WRITE setKernelData NOTIFY kernelDataChanged)
    Q_PROPERTY(AnnotationTypes type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int margin READ margin WRITE setMargin NOTIFY marginChanged)
    Q_PROPERTY(QRect annotationFrame READ annotationFrame WRITE setAnnotationFrame NOTIFY annotationFrameChanged)

public:
    Q_INVOKABLE void updateKernelRect();

    enum AnnotationTypes {
        AnnotationText = 0,
        AnnotationDate,
        AnnotationSignature,
        AnnotationInitials,
        AnnotationTypesMax
    }; Q_ENUM(AnnotationTypes);

    KSAnnotationItem(QQuickItem *parent = 0);
    ~KSAnnotationItem();

    const QString & kernelData() { return m_data; };

    AnnotationTypes type() const
    {
        return m_type;
    }

    int margin() const
    {
        return m_margin;
    }

    QRect annotationFrame() const
    {
        return m_annotationFrame;
    }

public slots:
    void setKernelData(const QString &data);
    void setType(AnnotationTypes type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(m_type);
    }

    void setMargin(int margin)
    {
        if (m_margin == margin)
            return;

        m_margin = margin;
        emit marginChanged(m_margin);
    }

    void setAnnotationFrame(QRect annotationFrame)
    {
        if (m_annotationFrame == annotationFrame)
            return;

        m_annotationFrame = annotationFrame;
        emit annotationFrameChanged(m_annotationFrame);
    }

signals:
    void kernelDataChanged();
    void typeChanged(AnnotationTypes type);

#if(DEBUG_TEST_REMOT_ENABLED)
public slots:
    void refrash();
#endif

    void marginChanged(int margin);

    void annotationFrameChanged(QRect annotationFrame);

private:
    void updateDataFromKernel();
    void updateFrameFromKernel();
#if(DEBUG_TEST_REMOT_ENABLED)

    QTimer *refrashTimer;
#endif

    bool isRectNeedUpdate;
    bool isKernelRectBusy;

    QRect kernelRect;
    AnnotationTypes m_type;
    int m_margin;
    QRect m_annotationFrame;

protected:
    QString m_data;
};

#endif // KSANNOTATIONITEM_H
