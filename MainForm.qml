import QtQuick 2.5
import QtQml 2.2
import QtQuick.Controls 2.0
import Theme 1.0

Rectangle {
    id: mainScreen
    objectName: "mainScreen"
    // Put the name of the QML files containing your pages (without the '.qml')
    property variant pagesList: ["MainScreenPages/DocumentsView",
        "MainScreenPages/TemplatesView",
        "MainScreenPages/ArchiveView",
        "MainScreenPages/MySignatureView",
        "MainScreenPages/ContactsView",
        "MainScreenPages/FeedbackView",
        "MainScreenPages/SettingsView"]
    // Set this property to another file name to change page
    property int currentPageIndex: 0

    Rectangle {
        id: rectangleLeftMenu
        objectName: "rectangleLeftMenu"
        width: 230
        color: Theme.blueColor
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        ListView {
            id: leftMenuList
            objectName: "leftMenuList"
            x: 0
            y: 174
            width: 230
            anchors.top: rectangleLogo.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.topMargin: 0
            model: ListModel {
                ListElement {
                    title: "Documents"
                    image: "resources/tabImg1.png"
                }
                ListElement {
                    title: "Templates"
                    image: "resources/tabImg1.png"
                }
                ListElement {
                    title: "Archive"
                    image: "resources/tabImg1.png"
                }
                ListElement {
                    title: "My signature"
                    image: "resources/tabImg1.png"
                }
                ListElement {
                    title: "Contacts"
                    image: "resources/tabImg1.png"
                }
                ListElement {
                    title: "Feedback"
                    image: "resources/tabImg1.png"
                }
                ListElement {
                    title: "Settings"
                    image: "resources/tabImg1.png"
                }
            }

            delegate: Rectangle {
                id: rectangle1
                width: 230
                height: 40
                color: (leftMenuList.currentIndex == index) ? "#ffffff" : "#00000000"
                border.width: 0
                Text {
                    color: (leftMenuList.currentIndex == index) ? Theme.blueColor : "#ffffff"
                    text: title
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 75
                    horizontalAlignment: Text.AlignLeft
                    styleColor: "#00000000"
                    style: Text.Sunken
                    font.weight: Font.Light
                    font.capitalization: Font.MixedCase
                    font.pointSize: 12
                    font.family: "Verdana"
                }

                Image {
                    id: image1
                    anchors.left: parent.left
                    anchors.leftMargin: 18
                    width: 40
                    height: 40
                    source: image
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        mainScreen.currentPageIndex = index
                        leftMenuList.currentIndex = index
                    }
                }
            }
        }

        Rectangle {
            id: rectangleLogo
            objectName: "rectangleLogo"
            width: 230
            height: 174
            color: "#00ffffff"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Image {
                id: imageLogo
                objectName: "imageLogo"
                width: 230
                height: 174
                anchors.verticalCenterOffset: 0
                anchors.horizontalCenterOffset: 0
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                transformOrigin: Item.Center
                clip: true
                fillMode: Image.Stretch
                z: 2
                source: "resources/logoImg.png"
            }
        }

        Rectangle {
            id: rectangleLock
            objectName: "rectangleLock"
            y: 432
            width: 230
            height: 48
            color: "#186bb4"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0

            Image {
                id: imageLock
                x: 19
                y: 8
                width: 16
                height: 16
                anchors.verticalCenter: parent.verticalCenter
                source: "resources/logoImg.png"
            }

            Text {
                id: textLockLabel
                x: 50
                y: 17
                height: 15
                color: "#ffffff"
                text: qsTr("Lock")
                styleColor: "#00000000"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 16
                anchors.right: parent.right
                anchors.rightMargin: 34
                anchors.left: parent.left
                anchors.leftMargin: 50
                font.pixelSize: 12
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    appController.lock();
                }
            }
        }
    }

    Rectangle {
        id: rectangleRightView
        objectName: "rectangleRightView"
        x: 230
        y: 0
        border.color: "#00000000"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: rectangleLeftMenu.right
        anchors.leftMargin: 0
        Repeater {
            id: pageRepeater
            anchors.topMargin: 30
            anchors.fill: rectangleRightView
            model: pagesList
            delegate: Loader {
                id: pageLoader
                source: "%1.qml".arg(modelData)
                active: false
                asynchronous: false
                anchors.fill: pageRepeater
                visible: (mainScreen.currentPageIndex === index)
                onVisibleChanged: {
                    loadIfNotLoaded()
                }
                Component.onCompleted: {
                    loadIfNotLoaded()
                }

                function loadIfNotLoaded() {
                    // to load the file at first show
                    if (visible && !active) {
                        active = true
                    }
                }
            }
        }

        Rectangle {
            id: loginBar
            height: 30
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Rectangle {
                id: loginButton
                width: 182
                color: "#f4f4f4"
                anchors.left: parent.left
                anchors.leftMargin: 28
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                border.color: "#00000000"
                Menu {
                        id: loginMenu
                        x:0
                        y:parent.height
                        width: parent.width
                        MenuItem { text: "Sign In" }
                        MenuItem { text: "Quit"; }
                    }

                Text {
                    id: loginLabel
                    x: 36
                    y: 9
                    width: 133
                    height: 15
                    text: qsTr("Alex Mnogosmyslov")
                    color: "#2980cc"
                    font.pixelSize: 12
                }

                MouseArea {
                    id: loginBarMouseArea
                    anchors.fill: parent
                    onClicked: {
                        loginMenu.open()
                    }
                }
            }

            CSTextFieldBtn
            {
                id: textField
                x: 226
                y: 8
                width: 143
                height: 0
                padding: -1
                font.pointSize: 10
                anchors.rightMargin: 90
                anchors.topMargin: 4
                anchors.bottomMargin: 4
                anchors.leftMargin: 230
                font.pixelSize: 12
            }
        }

    }

    //*
    //*/
}
