#ifndef KSASYNCPAGERESPONSE_H
#define KSASYNCPAGERESPONSE_H

#include <qquickimageprovider.h>
#include <QDebug>
#include <QImage>
#include "AppController.h"

class KSAsyncPageResponse : public QQuickImageResponse, public QRunnable
{
public:
        KSAsyncPageResponse(const QString &id, const QSize &requestedSize)
         : m_id(id), m_requestedSize(requestedSize), m_image(200, 300, QImage::Format_RGB32)
        {
            setAutoDelete(false);
        }

        QQuickTextureFactory *textureFactory() const
        {
            return QQuickTextureFactory::textureFactoryForImage(m_image);
        }

        void run()
        {
            int pageIndex = m_id.toInt();
            QImage *img = AppController::instance()->docController()->readPageImage(pageIndex, m_requestedSize);

            if(img == NULL)
            {
                qDebug() << "***WARNING!!! PageResponse: the document are not ready";
                m_image = QImage();
                emit finished();
            }
            m_image = img->copy();

            emit finished();
        }

        QString m_id;
        QSize m_requestedSize;
        QImage m_image;
};

#endif // KSASYNCPAGERESPONSE_H
