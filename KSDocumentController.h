#ifndef KSDOCUMENTCONTROLLER_H
#define KSDOCUMENTCONTROLLER_H

#include <QObject>
#include <QtQuick>
#include <QList>
#include <QHash>

class QPdfDocument;
class KSPageItem;

class KSDocumentController : public QQuickItem
{
    Q_OBJECT


public:

    Q_PROPERTY(QPdfDocument * pdf READ pdf WRITE setPdf NOTIFY pdfChanged)
    Q_PROPERTY(QString docId READ docId WRITE setDocId NOTIFY docIdChanged)
    Q_PROPERTY(qreal documentHeight READ documentHeight WRITE setDocumentHeight NOTIFY documentHeightChanged)
    Q_PROPERTY(qreal documentScale READ documentScale WRITE setDocumentScale NOTIFY documentScaleChanged)
    Q_INVOKABLE QSizeF pageSize(int pageIndex);
    Q_INVOKABLE void setPageActivationIndex(int pageActivationIndex);

    enum AnnotationContextIndex {
        AnnotationContextEdit = 0,
        AnnotationContextFont,
        AnnotationContextChangeParticipant,
        AnnotationContextDelete,
        AnnotationContextBold,
        AnnotationContextItalic,
        AnnotationContextAAdd,
        AnnotationContextASub
    }; Q_ENUM(AnnotationContextIndex);

    KSDocumentController(QQuickItem *parent = Q_NULLPTR);

    QString docId() const
    {
        return m_docId;
    }

    QPdfDocument * pdf() const
    {
        return m_pdf;
    }

    QImage *readPageImage(int pageIndex, QSize requestedSize);

    qreal documentHeight() const
    {
        return m_documentHeight;
    }

    qreal documentScale() const
    {
        return m_documentScale;
    }

public slots:
    void documentStatusChanged();
    void setDocId(QString docId);

    void setPdf(QPdfDocument * pdf)
    {
        if (m_pdf == pdf)
            return;

        m_pdf = pdf;
        emit pdfChanged(m_pdf);
    }

    void setDocumentHeight(qreal documentHeight)
    {
        qWarning("Floating point comparison needs context sanity check");
        if (qFuzzyCompare(m_documentHeight, documentHeight))
            return;

        m_documentHeight = documentHeight;
        emit documentHeightChanged(m_documentHeight);
    }

    void setDocumentScale(qreal documentScale)
    {
        qWarning("Floating point comparison needs context sanity check");
        if (qFuzzyCompare(m_documentScale, documentScale))
            return;

        m_documentScale = documentScale;
        emit documentScaleChanged(m_documentScale);
    }

signals:
    void imageChanged(int pageIndex, const QImage image);
    void pageReady(int pageIndex); // any changes on the page for preview
    void docIdChanged(QString docId);

    void pdfChanged(QPdfDocument * pdf);

    void documentHeightChanged(qreal documentHeight);

    void documentScaleChanged(qreal documentScale);

private:

    QString m_docId;
    QPdfDocument * m_pdf;
    QHash<int,QImage*> _imageCache;

    qreal m_documentHeight;
    qreal m_documentScale;
};

#endif // KSDOCUMENTCONTROLLER_H
