#ifndef KSGRAPHICEFFECTSHELPER_H
#define KSGRAPHICEFFECTSHELPER_H
#include <QImage>
#include "math.h"
#include <QDebug>
#include <QGenericMatrix>

inline QImage blurredImage(const QImage& image)
{

    QImage blur = image;

    QMatrix3x3 kernel;
    kernel(0, 0) = 1; kernel(0, 1) = 2; kernel(0, 2) = 1;
    kernel(1, 0) = 2; kernel(1, 1) = 4; kernel(1, 2) = 2;
    kernel(2, 0) = 1; kernel(2, 1) = 2; kernel(2, 2) = 1;
    float kernel_sum = 16.0;


    for(int i=1; i<image.width()-1; i++)
    {
        for(int j=1; j<image.height()-1; j++)
        {
            float red = 0, green = 0, blue = 0;

            // *****************************************************
            red =
                    kernel(0, 0) * qRed(image.pixel(i+1, j+1)) +
                    kernel(0, 1) * qRed(image.pixel(i, j+1)) +
                    kernel(0, 2) * qRed(image.pixel(i-1, j+1)) +

                    kernel(1, 0) * qRed(image.pixel(i+1, j)) +
                    kernel(1, 1) * qRed(image.pixel(i, j)) +
                    kernel(1, 2) * qRed(image.pixel(i-1, j)) +

                    kernel(2, 0) * qRed(image.pixel(i+1, j-1)) +
                    kernel(2, 1) * qRed(image.pixel(i, j-1)) +
                    kernel(2, 2) * qRed(image.pixel(i-1, j-1));

            // *****************************************************
            green =
                    kernel(0, 0) * qGreen(image.pixel(i+1, j+1)) +
                    kernel(0, 1) * qGreen(image.pixel(i, j+1)) +
                    kernel(0, 2) * qGreen(image.pixel(i-1, j+1)) +

                    kernel(1, 0) * qGreen(image.pixel(i+1, j)) +
                    kernel(1, 1) * qGreen(image.pixel(i, j)) +
                    kernel(1, 2) * qGreen(image.pixel(i-1, j)) +

                    kernel(2, 0) * qGreen(image.pixel(i+1, j-1)) +
                    kernel(2, 1) * qGreen(image.pixel(i, j-1)) +
                    kernel(2, 2) * qGreen(image.pixel(i-1, j-1));

            // *****************************************************
            blue =
                    kernel(0, 0) * qBlue(image.pixel(i+1, j+1)) +
                    kernel(0, 1) * qBlue(image.pixel(i, j+1)) +
                    kernel(0, 2) * qBlue(image.pixel(i-1, j+1)) +

                    kernel(1, 0) * qBlue(image.pixel(i+1, j)) +
                    kernel(1, 1) * qBlue(image.pixel(i, j)) +
                    kernel(1, 2) * qBlue(image.pixel(i-1, j)) +

                    kernel(2, 0) * qBlue(image.pixel(i+1, j-1)) +
                    kernel(2, 1) * qBlue(image.pixel(i, j-1)) +
                    kernel(2, 2) * qBlue(image.pixel(i-1, j-1));

            blur.setPixel(i,j, qRgba(red/kernel_sum, green/kernel_sum, blue/kernel_sum, qAlpha(image.pixel(i, j))));

        }
    }

    return blur;
}

inline QImage blurredImage(const QImage& image, const QRect& rect, int radius, bool alphaOnly = false)
{
    int tab[] = { 14, 10, 8, 6, 5, 5, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2 };
    int alpha = (radius < 1)  ? 16 : (radius > 17) ? 1 : tab[radius-1];

    QImage result = image.convertToFormat(QImage::Format_ARGB32_Premultiplied);
    int r1 = rect.top();
    int r2 = rect.bottom();
    int c1 = rect.left();
    int c2 = rect.right();

    int bpl = result.bytesPerLine();
    int rgba[4];
    unsigned char* p;

    int i1 = 0;
    int i2 = 3;

    if (alphaOnly)
        i1 = i2 = (QSysInfo::ByteOrder == QSysInfo::BigEndian ? 0 : 3);

    for (int col = c1; col <= c2; col++) {
        p = result.scanLine(r1) + col * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p += bpl;
        for (int j = r1; j < r2; j++, p += bpl)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    for (int row = r1; row <= r2; row++) {
        p = result.scanLine(row) + c1 * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p += 4;
        for (int j = c1; j < c2; j++, p += 4)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    for (int col = c1; col <= c2; col++) {
        p = result.scanLine(r2) + col * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p -= bpl;
        for (int j = r1; j < r2; j++, p -= bpl)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    for (int row = r1; row <= r2; row++) {
        p = result.scanLine(row) + c2 * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p -= 4;
        for (int j = c1; j < c2; j++, p -= 4)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    return result;
}

template<class T>
inline const T& kClamp( const T& x, const T& low, const T& high )
{
    if ( x < low )       return low;
    else if ( high < x ) return high;
    else                 return x;
}

inline
int changeBrightness( int value, int brightness )
{
    return kClamp( value + brightness * 255 / 100, 0, 255 );
}

inline
int changeContrast( int value, int contrast )
{
    return kClamp((( value - 127 ) * contrast / 100 ) + 127, 0, 255 );
}

inline
int changeGamma( int value, int gamma )
{
    return kClamp( int( pow( value / 255.0, 100.0 / gamma ) * 255 ), 0, 255 );
}

inline
int changeUsingTable( int value, const int table[] )
{
    return table[ value ];
}

inline int compare(const void *a, const void *b)
{
    return int(*(char*)a -*(char*)b);
}

inline int realX(int x, const QImage& image)
{
    if (x < 0) {
        return realX(-x - 1, image );
    } else if (x >= image.width()){
        return realX(2 * image.width() - 1 - x, image );
    } else {
        return x;
    }
}


/* for realPixel() */
inline int realY(int y, const QImage& image)
{
    if  (y < 0) {
        return realY(-y - 1, image );
    } else if (y >= image.height()) {
        return realY(2 * image.height() - 1 - y, image );
    } else {
        return y;
    }
}


/* Realization of median filter*/
inline QImage medianProcess(QImage image, int size)
{


    static char *rArray = new char[size*size];
    static char *gArray = new char[size*size];
    static char *bArray = new char[size*size];
    int halfSize = (size%2 == 0) ? size/2 - 1 : (size - 1) / 2;

    QImage imageBuf = image;

    for(int y = 0; y < imageBuf.height(); y++) {
        for(int x = 0; x < imageBuf.width(); x++) {
            for(int m = 0; m < size; m++) {
                int j = y + m - halfSize;
                for(int n = 0; n < size; n++) {
                    int i = x + n - halfSize;
                    rArray[size*m + n] = qRed(imageBuf.pixel(realX(i, image ), realY(j, image )));
                    gArray[size*m + n] = qGreen(imageBuf.pixel(realX(i, image ), realY(j, image )));
                    bArray[size*m + n] = qBlue(imageBuf.pixel(realX(i, image ), realY(j, image )));
                }
            }
            qsort(rArray, size*size, sizeof(char), compare);
            qsort(gArray, size*size, sizeof(char), compare);
            qsort(bArray, size*size, sizeof(char), compare);
            int median = (size%2 == 0) ? (size*size - 1)/2: ((size-1)*(size-1)-1)/2;
            int red = rArray[median];
            int green = gArray[median];
            int blue = bArray[median];
            imageBuf.setPixel(x, y, qRgba(red, green, blue,qAlpha(imageBuf.pixel(x, y))));
        }
    }
//    delete [] rArray;
//    delete [] gArray;
//    delete [] bArray;

    return imageBuf;
}

inline QImage cleanUpAlpha(QImage pImage)
{
    for(int x=0; x<pImage.width(); x++)
        for(int y=0; y<pImage.height(); y++)
            pImage.setPixel(x, y, qAlpha(pImage.pixel(x,y)) == 0
                            ? qRgba(255, 255, 255, 0)
                            : pImage.pixel(x,y));

    return pImage;
}

inline QImage makeGray(QImage pImage)
{
    QImage result(pImage.size(),QImage::Format_RGBA8888);
    result.fill(Qt::transparent);
    for(int x=0; x<pImage.width(); x++)
    {
        for(int y=0; y<pImage.height(); y++)
        {
            QRgb pixel = pImage.pixel(x, y);
            if(qAlpha(pixel) > 0)
            {
                int gray = qGray(pixel);
                result.setPixel(x, y, qRgba(gray, gray, gray, qAlpha(pixel)));
            }
        }
    }
    return result;
}

inline QImage getThresholdBW(QImage pImage, int threshold, bool isUpLimit)
{
    QImage result(pImage.size(),QImage::Format_RGBA8888);
    result.fill(Qt::transparent);
    for(int x=0; x<pImage.width(); x++)
    {
        for(int y=0; y<pImage.height(); y++)
        {
            QRgb pixel = pImage.pixel(x, y);
            if(qAlpha(pixel) > 0)
            {
                int gray = qGray(pixel);
                result.setPixel(x, y, (gray > threshold && isUpLimit) || (gray < threshold && !isUpLimit) ? qRgba(255, 255, 255, 0) : qRgba(0, 0, 0, 255));
            }
        }
    }
    return result;
}


inline int getMedianGrey(QImage pImage, float level)// +-0..1
{
    int max = 0;
    int min = 255;
    int sum = 0;
    int count = 0;
    for(int x=0; x<pImage.width(); x++)
    {
        for(int y=0; y<pImage.height(); y++)
        {
            if(qAlpha(pImage.pixel(x, y)) > 0)
            {
                int vlue = qGray(pImage.pixel(x, y));
                sum += vlue;
                count++;

                if(vlue > max)
                {
                    max = vlue;
                }

                if(vlue < min)
                {
                    min = vlue;
                }
            }
        }
    }

    float middle = (sum/count);
    if(level > 0)
    {
        sum = middle + (max - middle)*level;
    }
    else if(level < 0)
    {
        sum = middle - (middle - min)*level;
    }
    else
    {
        sum = middle;
    }

    return sum;
}


template< int operation( int, int ) >
static
QImage changeImage( const QImage& image, int value )
{
    QImage im = image;
    im.detach();
    if( im.colorCount() == 0 ) /* truecolor */
    {
        //        if( im.format() != QImage::Format_RGB32 ) /* just in case */
        //            im = im.convertToFormat( QImage::Format_RGB32 );
        int table[ 256 ];
        for( int i = 0;
             i < 256;
             ++i )
            table[ i ] = operation( i, value );
        if( im.hasAlphaChannel() )
        {
            for( int y = 0;
                 y < im.height();
                 ++y )
            {
                QRgb* line = reinterpret_cast< QRgb* >( im.scanLine( y ));
                for( int x = 0;
                     x < im.width();
                     ++x )
                    line[ x ] = qRgba( changeUsingTable( qRed( line[ x ] ), table ),
                                       changeUsingTable( qGreen( line[ x ] ), table ),
                                       changeUsingTable( qBlue( line[ x ] ), table ),
                                       qAlpha( line[ x ] ) == 0 ? 0 : changeUsingTable( qAlpha( line[ x ] ), table ));
            }
        }
        else
        {
            for( int y = 0;
                 y < im.height();
                 ++y )
            {
                QRgb* line = reinterpret_cast< QRgb* >( im.scanLine( y ));
                for( int x = 0;
                     x < im.width();
                     ++x )
                    line[ x ] = qRgb( changeUsingTable( qRed( line[ x ] ), table ),
                                      changeUsingTable( qGreen( line[ x ] ), table ),
                                      changeUsingTable( qBlue( line[ x ] ), table ));
            }
        }
    }
    else
    {
        QVector<QRgb> colors = im.colorTable();
        for( int i = 0;
             i < im.colorCount();
             ++i )
            colors[ i ] = qRgb( operation( qRed( colors[ i ] ), value ),
                                operation( qGreen( colors[ i ] ), value ),
                                operation( qBlue( colors[ i ] ), value ));
    }
    return im;
}

inline QImage applyLevels(QImage rgb, const float exposure, const float brightness, const float contrast, const float saturation)
{
    float R, G, B;

    unsigned int pixelIndex = 0;

    float exposureFactor   = powf(2.0f, exposure);
    float brightnessFactor = brightness / 10.0f;
    float contrastFactor   = contrast > 0.0f ? contrast : 0.0f;
    float saturationFactor = 1.0f - saturation;

    for (int y = 0; y < rgb.height(); y++)
    {
        for (int x = 0; x < rgb.width(); x++)
        {
            QRgb pixel = rgb.pixel(x, y);

            R = qRed(pixel) / 255.0f;
            G = qGreen(pixel) / 255.0f;
            B = qBlue(pixel) / 255.0f;

            // Clamp values

            R = R > 1.0f ? 1.0f : R < 0.0f ? 0.0f : R;
            G = G > 1.0f ? 1.0f : G < 0.0f ? 0.0f : G;
            B = B > 1.0f ? 1.0f : B < 0.0f ? 0.0f : B;

            // Exposure

            R *= exposureFactor;
            G *= exposureFactor;
            B *= exposureFactor;

            // Contrast

            R = (((R - 0.5f) * contrastFactor) + 0.5f);
            G = (((G - 0.5f) * contrastFactor) + 0.5f);
            B = (((B - 0.5f) * contrastFactor) + 0.5f);

            // Saturation

            float gray = (R * 0.3f) + (G * 0.59f) + (B * 0.11f);
            R = (gray * saturationFactor) + R * saturation;
            G = (gray * saturationFactor) + G * saturation;
            B = (gray * saturationFactor) + B * saturation;

            // Brightness

            R += brightnessFactor;
            G += brightnessFactor;
            B += brightnessFactor;

            // Clamp values

            R = R > 1.0f ? 1.0f : R < 0.0f ? 0.0f : R;
            G = G > 1.0f ? 1.0f : G < 0.0f ? 0.0f : G;
            B = B > 1.0f ? 1.0f : B < 0.0f ? 0.0f : B;

            // Store new pixel value

            R *= 255.0f;
            G *= 255.0f;
            B *= 255.0f;

            rgb.setPixel(x,y,qRgba(R,G,B,qAlpha(pixel)));

            pixelIndex++;
        }
    }

    return rgb;
}

// brightness is multiplied by 100 in order to avoid floating point numbers
inline QImage changeBrightness( const QImage& image, int brightness )
{
    if( brightness == 0 ) // no change
        return image;
    return changeImage< changeBrightness >( image, brightness );
}


// contrast is multiplied by 100 in order to avoid floating point numbers
inline QImage changeContrast( const QImage& image, int contrast )
{
    if( contrast == 100 ) // no change
        return image;
    return changeImage< changeContrast >( image, contrast );
}

// gamma is multiplied by 100 in order to avoid floating point numbers
inline QImage changeGamma( const QImage& image, int gamma )
{
    if( gamma == 100 ) // no change
        return image;
    return changeImage< changeGamma >( image, gamma );
}
#endif // KSGRAPHICEFFECTSHELPER_H
