#ifndef KSPAGERENDERSESSION_H
#define KSPAGERENDERSESSION_H

#include <QObject>
#include "KSPageRenderWorker.h"
#include <QPdfDocument>
#include <QHash>

class KSPageRenderSession : public QObject
{
    Q_OBJECT
public:
    explicit KSPageRenderSession(QObject *parent = nullptr);
    ~KSPageRenderSession();

    void setDocument(QPdfDocument *document) {
        stopThreads();
        m_pdf = document;
    }

    static KSPageRenderSession *instance() {
        static KSPageRenderSession *session = new KSPageRenderSession();
        return session;
    }

    bool isImageReady(int pageIndex, qreal zoom)
    {
        return _zoomCache.contains(pageIndex) && _zoomCache[pageIndex] >= zoom;
    };

    const QImage & getImage(int pageIndex)
    {
        if(_imageCache.contains(pageIndex))
        {
            return *_imageCache[pageIndex];
        }
        else
        {
            return QImage();
        }
    };

    QThread* addThread(int page, qreal zoom);
    void stopThreads();
public slots:
    void  pageLoaded(int page, qreal zoom, const QImage image);

signals:
    void imageReady(int pageIndex, const QImage);

private:

    QPdfDocument * m_pdf;
    QHash<int,QImage*> _imageCache;
    QHash<int,qreal> _zoomCache;//QThread

signals:
    void stopAll(); //остановка всех потоков
};

#endif // KSPAGERENDERSESSION_H
