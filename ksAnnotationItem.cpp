#include "ksAnnotationItem.h"
#include "theme.h"
#include <QTimer>
#include <QtCore>

//KSAnnotationItem::KSAnnotationItem()
//{

//}

KSAnnotationItem::KSAnnotationItem(QQuickItem *parent)
    : QQuickItem(parent)
    , m_data("image://myprovider/200/300/?image=1")
    , kernelRect(0,0,100,100)
    , m_annotationFrame(10 + qrand()%200,10 + qrand()%200,50,50)
    , isKernelRectBusy(false)
    , m_type(AnnotationTypes::AnnotationSignature)
    , m_margin(0)

{
    updateKernelRect();

#if(DEBUG_TEST_REMOT_ENABLED)
    refrashTimer = new QTimer(this);
    connect(refrashTimer, SIGNAL(timeout()), this, SLOT(refrash()));
    refrashTimer->start(1000 + qrand()%50000);
#endif
};

KSAnnotationItem::~KSAnnotationItem()
{
#if(DEBUG_TEST_REMOT_ENABLED)
    delete refrashTimer;
#endif
}

void KSAnnotationItem::setKernelData(const QString &data)
{
    m_data = data;
    emit kernelDataChanged();
}

void KSAnnotationItem::updateKernelRect()
{
    isRectNeedUpdate = false;

    kernelRect = QRect(x(),y(),width(),height());
//    qDebug() << "KSAnnotationItem::updateKernelRect("
//             << kernelRect.x()
//             << kernelRect.y()
//             << kernelRect.width()
//             << kernelRect.height()
//             << ")";
}

void KSAnnotationItem::updateDataFromKernel()
{
    QString data = "image://myprovider/200/300?image=%1";
    int k = qrand()%1000;
    data = data.arg(QString::number(k));
    setKernelData(data);
}

void KSAnnotationItem::updateFrameFromKernel()
{
    setWidth(kernelRect.width());
    setHeight(kernelRect.height());
    setX(kernelRect.x());
    setY(kernelRect.y());
}

#if(DEBUG_TEST_REMOT_ENABLED)

void KSAnnotationItem::refrash()
{
    updateDataFromKernel();
    updateFrameFromKernel();
}

#endif
