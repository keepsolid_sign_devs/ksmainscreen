#ifndef KSANNOTATIONDATE_H
#define KSANNOTATIONDATE_H


#include "KSAnnotationText.h"

class KSAnnotationDate : public KSAnnotationText
{
public:
    KSAnnotationDate(QQuickItem *parent = 0);
};

#endif // KSANNOTATIONDATE_H
