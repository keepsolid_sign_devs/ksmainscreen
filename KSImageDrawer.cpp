#include "KSImageDrawer.h"

KSImageDrawer::KSImageDrawer(QQuickItem *parent) : QQuickPaintedItem(parent), m_image(QImage())
{
    setMipmap(true);
}

void KSImageDrawer::paint(QPainter *painter)
{
    if( m_image.isNull() == false)
    {
        QRectF bound = boundingRect();
        painter->drawImage(bound, m_image);
    }
}
