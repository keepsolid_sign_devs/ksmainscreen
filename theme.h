#ifndef THEME_H
#define THEME_H

#include <QObject>
#include <QQmlEngine>
#include <QColor>

class Theme: public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Theme)
    Q_PROPERTY(QColor blueColor READ blueColor WRITE setBlueColor NOTIFY blueColorChanged);
    Q_PROPERTY(QColor whiteColor READ whiteColor WRITE setWhiteColor NOTIFY whiteColorChanged);
    Q_PROPERTY(QColor whiteTwoColor READ whiteTwoColor WRITE setWhiteTwoColor NOTIFY whiteTwoColorChanged);
    Q_PROPERTY(QColor greyishBrownColor READ greyishBrownColor WRITE setGreyishBrownColor NOTIFY greyishBrownColorChanged);
    Q_PROPERTY(QColor warmGreyColor READ warmGreyColor WRITE setWarmGreyColor NOTIFY warmGreyColorChanged);
    Q_PROPERTY(QColor whiteThree READ whiteThree WRITE setWhiteThree NOTIFY whiteThreeChanged);

    Q_PROPERTY(QColor authorAnnotationColor READ authorAnnotationColor WRITE setAuthorAnnotationColor NOTIFY authorAnnotationColorChanged);

    Q_PROPERTY(QColor blueColorAlph5 READ blueColorAlph5 WRITE setBlueColorAlph5 NOTIFY blueColorAlph5Changed)

    Q_PROPERTY(int pageInset READ pageInset WRITE setPageInset NOTIFY pageInsetChanged)

    Theme();

public:

    QColor whiteThree() const { return m_whiteThree; };
    void setWhiteThree(QColor color) { m_whiteThree = color; };

    QColor blueColor() const { return m_blueColor; };
    void setBlueColor(QColor color) { m_blueColor = color; };

    QColor whiteColor() const { return m_whiteColor; };
    void setWhiteColor(QColor color) { m_whiteColor = color; };

    QColor whiteTwoColor() const { return m_whiteTwoColor; };
    void setWhiteTwoColor(QColor color) { m_whiteTwoColor = color; };

    QColor greyishBrownColor() const { return m_greyishBrownColor; };
    void setGreyishBrownColor(QColor color) { m_greyishBrownColor = color; };

    QColor warmGreyColor() const { return m_warmGreyColor; };
    void setWarmGreyColor(QColor color) { m_warmGreyColor = color; };

    QColor blueColorAlph5() const { return m_blueColorAlph5; };
    void setBlueColorAlph5(QColor color) { m_blueColorAlph5 = color; };

    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
    {
        Q_UNUSED(engine);
        Q_UNUSED(scriptEngine);

        return instance();
    }

    static Theme *instance()
    {
        static Theme *theme = new Theme();
        return theme;
    }

    QColor authorAnnotationColor() const
    {
        return m_authorAnnotationColor;
    }

    int pageInset() const
    {
        return m_pageInset;
    }

public slots:
    void setAuthorAnnotationColor(QColor authorAnnotationColor)
    {
        if (m_authorAnnotationColor == authorAnnotationColor)
            return;

        m_authorAnnotationColor = authorAnnotationColor;
        emit authorAnnotationColorChanged(m_authorAnnotationColor);
    }

    void setPageInset(int pageInset)
    {
        if (m_pageInset == pageInset)
            return;

        m_pageInset = pageInset;
        emit pageInsetChanged(m_pageInset);
    }

signals:
    void blueColorChanged();
    void whiteColorChanged();
    void whiteTwoColorChanged();
    void greyishBrownColorChanged();
    void warmGreyColorChanged();
    void whiteThreeChanged();
    void blueColorAlph5Changed();

    void authorAnnotationColorChanged(QColor authorAnnotationColor);

    void pageInsetChanged(int pageInset);

private:
    QColor m_blueColor;
    QColor m_whiteColor;
    QColor m_whiteTwoColor;
    QColor m_greyishBrownColor;
    QColor m_warmGreyColor;
    QColor m_whiteThree;
    QColor m_blueColorAlph5;

    QColor m_authorAnnotationColor;
    int m_pageInset;
};

#endif // THEME_H
