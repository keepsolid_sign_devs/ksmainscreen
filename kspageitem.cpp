#include "kspageitem.h"
#include "theme.h"
#include "pagerenderer.h"
#include "KSDocumentController.h"
#include "AppController.h"
#include <QtGlobal>
#include "KSPageRenderSession.h"

KSPageItem::KSPageItem(QQuickItem *parent)
    : QQuickItem(parent)
    , m_annotationsModel(new KSAnnotationListModel(this))
{

}

KSPageItem::~KSPageItem()
{
    delete m_annotationsModel;
//    delete m_pageRenderer;
}
