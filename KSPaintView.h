/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TEXTBALLOON_H
#define TEXTBALLOON_H

#include <QtQuick>
#include "roundedpolygon.h"
#include <QGraphicsEffect>

#define SIGNATURE_WIDTH 1200
#define SIGNATURE_HEIGHT 900

class KSPaintView : public QQuickPaintedItem
{
    Q_OBJECT
public:
    enum PaintViewBrushStyle {
        PaintViewPen = 0,
        PaintViewNarrow,
        PaintViewEarase
    }; Q_ENUM(PaintViewBrushStyle);
    
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QImage signatureImage READ signatureImage WRITE setSignatureImage NOTIFY signatureImageChanged)
    Q_PROPERTY(PaintViewBrushStyle brushStyle READ brushStyle WRITE setBrushStyle NOTIFY brushStyleChanged)
    Q_INVOKABLE void startPoint(QPoint point);
    Q_INVOKABLE void nextPoint(QPoint point);
    Q_INVOKABLE void endPoint(QPoint point);
    Q_INVOKABLE void clear();
    Q_INVOKABLE void save();
    Q_INVOKABLE void importFile(QString url);
    Q_PROPERTY(int implicitWidth READ implicitWidth NOTIFY implicitWidthChanged)
    Q_PROPERTY(int implicitHeight READ implicitHeight NOTIFY implicitHeightChanged)
    Q_PROPERTY(qreal scale READ scale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(int textureWidth READ textureWidth NOTIFY textureWidthChanged)
    Q_PROPERTY(int textureHeight READ textureHeight NOTIFY textureHeightChanged)

    KSPaintView(QQuickItem *parent = 0);
    void paint(QPainter *painter);

    QImage signatureImage() const { return m_signatureImage; };
    void makeFromImage(QImage source);

    QUrl source() const
    {
        return m_source;
    }

    QColor color() const
    {
        return m_color;
    }

    PaintViewBrushStyle brushStyle() const
    {
        return m_brushStyle;
    }

    int implicitWidth() const;
    int implicitHeight() const;

    qreal scale() const
    {
        return m_scale;
    }

    int textureWidth() const;
    int textureHeight() const;

Q_SIGNALS:
    void signatureImageChanged();

    void sourceChanged(QUrl source);

    void colorChanged(QColor color);

    void brushStyleChanged(PaintViewBrushStyle brushStyle);

    void implicitWidthChanged(int implicitWidth);

    void implicitHeightChanged(int implicitHeight);

    void scaleChanged(qreal scale);

    void textureWidthChanged(int textureWidth);

    void textureHeightChanged(int textureHeight);

public Q_SLOTS:
    void loadImage(QUrl source);
    void setSignatureImage(QImage image)
    {
        if (m_signatureImage == image)
            return;

        m_signatureImage = image;
        emit signatureImageChanged();
    };
    void setSource(QUrl source)
    {
        if (m_source == source)
            return;

        m_source = source;
        emit textureWidthChanged(textureWidth());
        emit textureWidthChanged(textureHeight());
        emit sourceChanged(m_source);
    }

    void setColor(QColor color);

    void setBrushStyle(PaintViewBrushStyle brushStyle)
    {
        if (m_brushStyle == brushStyle)
            return;

        m_brushStyle = brushStyle;
        emit brushStyleChanged(m_brushStyle);
    }

    void setScale(qreal scale);

private:
    QImage applyEffectToImage(QImage src, QGraphicsEffect *effect);

    void applyImage();
    qreal xOffset();
    qreal yOffset();
    void drawPath(QPainter *painter);
    void drawSignature(QPainter *painter);
    void addUIPoint(QPoint point);

    RoundedPolygon poly;
    qreal polyWidth;
    qreal polyHeight;
    QImage m_signatureImage;


    QUrl m_source;
    QColor m_color;

    qreal m_scale;
    PaintViewBrushStyle m_brushStyle;
};


#endif
