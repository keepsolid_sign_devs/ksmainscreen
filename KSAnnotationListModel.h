#ifndef KSANNOTATIONLISTMODEL_H
#define KSANNOTATIONLISTMODEL_H

#include <QtQuick>
#include "ksAnnotationItem.h"

class KSAnnotationListModel : public QAbstractListModel
{
    Q_OBJECT

public:

    enum AnnotationItemRoles {
        amFile = Qt::UserRole + 1,
        amType,
        amParticipant
    };

    Q_INVOKABLE QVariantMap get(int row);

    KSAnnotationListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;



    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,

                 int role = Qt::EditRole);

    QModelIndex index(int row, int column,
                         const QModelIndex &parent = QModelIndex()) const;

    bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
    bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());

protected:
    virtual QHash<int,QByteArray> roleNames() const;
private:


    QList<int> m_annotations;
};

#endif // KSANNOTATIONLISTMODEL_H
