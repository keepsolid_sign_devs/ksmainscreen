#include "theme.h"
#include <QDebug>

Theme::Theme() : m_blueColor("#2980cc"),
    m_whiteColor("#f9f9f9"),
    m_whiteTwoColor("#dbdbdb"),
    m_greyishBrownColor("#444444"),
    m_warmGreyColor("#8e8e8e"),
    m_whiteThree("#ebebeb"),
    m_blueColorAlph5("#2980cc"),
    m_authorAnnotationColor("#5a8f19fb"),
    m_pageInset(10)
{
    m_blueColorAlph5.setAlpha(50);
}
